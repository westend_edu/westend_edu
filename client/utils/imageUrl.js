const imageUrl =
  process.env.NODE_ENV === "production"
    ? "https://thawing-springs-86554.herokuapp.com/views/uploads"
    : "http://localhost:5050/views/uploads";

export default imageUrl;
