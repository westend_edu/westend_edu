import React, { useState } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);
import baseUrl from "../../utils/baseUrl";

const alertContent = () => {
  MySwal.fire({
    title: "Congratulations!",
    text: "Your message was successfully sent. We will get back to you soon",
    icon: "success",
    timer: 5000,
    timerProgressBar: true,
    showConfirmButton: false,
  });
};

// Form initial state
const INITIAL_STATE = {
  name: "",
  email: "",
  number: "",
  text: "",
};

const ContactForm = () => {
  const [contact, setContact] = useState(INITIAL_STATE);
  const [loading, setLoading] = useState(false);

  const { register, handleSubmit, errors } = useForm();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setContact((prevState) => ({ ...prevState, [name]: value }));
  };

  const onSubmit = async (e) => {
    // e.preventDefault();
    setLoading(true);
    try {
      const url = `${baseUrl}/auth/contact`;
      const { name, email, number, text } = contact;
      const payload = { name, email, number, text };
      await axios.post(url, payload);
      setLoading(false);

      setContact(INITIAL_STATE);
      alertContent();
    } catch (error) {
      setLoading(false);

      console.log(error);
    }
  };

  return (
    <div className="contact-form">
      <h2>Ready to Get Started?</h2>
      <p>
        Your email address will not be published. Required fields are marked *
      </p>

      <form id="contactForm" onSubmit={handleSubmit(onSubmit)}>
        <div className="row">
          <div className="col-lg-6 col-md-6">
            <div className="form-group">
              <input
                type="text"
                name="name"
                placeholder="Your Name"
                value={contact.name}
                onChange={handleChange}
                ref={register({ required: true })}
              />
              <div className="invalid-feedback" style={{ display: "block" }}>
                {errors.name && "Name is required."}
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-md-6">
            <div className="form-group">
              <input
                type="text"
                name="email"
                placeholder="Your email address"
                value={contact.email}
                onChange={handleChange}
                ref={register({ required: true, pattern: /^\S+@\S+$/i })}
              />
              <div className="invalid-feedback" style={{ display: "block" }}>
                {errors.email && "Email is required."}
              </div>
            </div>
          </div>

          <div className="col-lg-12 col-md-6">
            <div className="form-group">
              <input
                type="text"
                name="number"
                placeholder="Your phone number"
                value={contact.number}
                onChange={handleChange}
                ref={register({ required: true })}
              />
              <div className="invalid-feedback" style={{ display: "block" }}>
                {errors.number && "Number is required."}
              </div>
            </div>
          </div>

          <div className="col-lg-12 col-md-12">
            <div className="form-group">
              <textarea
                name="text"
                cols="30"
                rows="5"
                placeholder="Write your message..."
                value={contact.text}
                onChange={handleChange}
                ref={register({ required: true })}
              />
              <div className="invalid-feedback" style={{ display: "block" }}>
                {errors.text && "Text body is required."}
              </div>
            </div>
          </div>

          <div className="col-lg-12 col-sm-12">
            <button type="submit" className="no-icon default-btn">
              {loading ? "SUBMITTING...." : "SUBMIT"} <span></span>
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default ContactForm;
