import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

export default function Checkboxes(props) {
  return (
    <div>
      <FormControlLabel
        control={
          <GreenCheckbox
            name={props.name}
            checked={props.value}
            onChange={props.onChange}
          />
        }
        label={props.label}
      />
    </div>
  );
}
