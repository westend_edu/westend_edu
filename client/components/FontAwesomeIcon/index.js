import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export const FontAwesome = (props) => {
  return <FontAwesomeIcon icon={props.icon} size={props.size} className={props.className} />;
};
export default FontAwesomeIcon;
