import React from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap";
const Example = (props) => {
  return (
    <div>
      <Card>
        <CardImg top width="100%" src={props.src} alt={props.alt} />
      </Card>
    </div>
  );
};

export default Example;
