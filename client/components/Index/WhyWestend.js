import React from "react";
const WhyWestend = (props) => {
  return (
    <div className="funfacts-list " >
       <div className="row IconAnimate" >
        <div
          onClick={() => props.toggle(1)}
          className="col-lg-6 col-md-6 col-sm-6"
        >
          <div className="single-funfacts-box" >
            <h3>100%</h3>
            <p>Transparent Process</p>
          </div>
        </div>

        <div
          onClick={() => props.toggle(2)}
          className="col-lg-6 col-md-6 col-sm-6"
        >
          <div className="single-funfacts-box">
            <h3>15+</h3>
            <p>Bank Parners</p>
          </div>
        </div>

        <div
          onClick={() => props.toggle(3)}
          className="col-lg-6 col-md-6 col-sm-6"
        >
          <div className="single-funfacts-box">
          <h3>Lowest</h3>
            <p>Interest Rates</p>
          </div>
        </div>

        <div
          onClick={() => props.toggle(4)}
          className="col-lg-6 col-md-6 col-sm-6"
        >
          <div className="single-funfacts-box">
          <h3>24*7</h3>
            <p>Support and Guidance</p>
          </div>
        </div>
        <div
          onClick={() => props.toggle(5)}
          className="col-lg-6 col-md-6 col-sm-6"
        >
          <div className="single-funfacts-box">
            <h3>Unsecured</h3>
            <p>Loan Options</p>
          </div>
        </div>
        <div
          onClick={() => props.toggle(6)}
          className="col-lg-6 col-md-6 col-sm-6"
        >
          <div className="single-funfacts-box">
            <h3>Free</h3>
            <p>Conlsultation</p>
          </div>
        </div>
      </div>
  
      
    </div>
  );
};

export default WhyWestend;
