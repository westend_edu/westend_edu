import React from "react";
import Link from "next/link";

const Instance = () => {
  const [autoPlay, setautoPlay] = React.useState(false);

  const timeoutRef = React.useRef(null);

  React.useEffect(() => {
    document.addEventListener("scroll", () => {
      if (window.scrollY > 2070) {
        setautoPlay(true);
      } else {
        setautoPlay(false);
      }
    });
  }, []);

  return (
    <div className="get-instant-courses-area-two">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-6 col-md-12">
            <div className="get-instant-courses-content-style-two">
              <span className="sub-title">Welcome to New Possibilities</span>
              <h2>How Westend Works</h2>
              <ul className="List">
                <li>
                  <div>
                    <span>1</span>
                    <div className="firstStepCenter">
                      Register with us and submit your application.
                    </div>
                  </div>
                </li>
                <li>
                  <div>
                    <span>2</span>We'll appoint a Loan Officer to evaluate your
                    profile and understand your education loan requirement.
                  </div>
                </li>
                <li>
                  <div>
                    <span>3</span>You can continue to stay in touch with your
                    Loan Assistant to follow up on your loan sanction and
                    disbursement process.
                  </div>
                </li>
                <li>
                  <div>
                    <span>4</span>After the initial disbursement, your assistant
                    will also help you in processing future disbursements.
                  </div>
                </li>
              </ul>
              <Link href="/signup">
                <a className="default-btn">
                  <i className="flaticon-user"></i>Apply Now<span></span>
                </a>
              </Link>
            </div>
          </div>
          <div className="col-lg-6 col-md-12">
            <video muted loop={true} autoPlay={autoPlay} controls>
              <source src="/video/video1.mp4" />
            </video>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Instance;
