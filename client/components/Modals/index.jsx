import React from "react"
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap"

export default function index(props) {
  return (
    <div>
      <Modal
        size={props.size?props.size:'lg'}
        isOpen={props.modal}
        toggle={props.toggle}
        className={props.className}
      >
        <ModalHeader toggle={props.toggle}>{props.title}</ModalHeader>
        <ModalBody>
          {props.content}
        </ModalBody>
        
      </Modal>
    </div>
  )
}
