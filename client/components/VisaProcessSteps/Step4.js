import React, { Fragment, useEffect, useState } from "react"
import { Dialog, Listbox, RadioGroup, Transition } from "@headlessui/react"
import { FiCheck, FiChevronDown, FiSearch } from "react-icons/fi"
import { BsArrowLeft, BsArrowRight } from "react-icons/bs"
import { Controller, useForm } from "react-hook-form"
import { useStateMachine } from "little-state-machine"
import updateAction from "./action/updateAction"

const countries = [
	{ id: 1, name: "Grade 12" },
	{ id: 2, name: "Undergraduate Diploma" },
]

const masters = [
	{ id: 1, name: "Undergraduate Degree" },
	{ id: 2, name: "Postgraduate Degree" },
	{ id: 3, name: "Undergraduate Diploma" },
	{ id: 4, name: "Postgraduate Diploma" },
]

const expYears = [
	{ id: 1, years: "0 to 1 Year" },
	{ id: 2, years: "1 to 2 Years" },
	{ id: 3, years: "2 to 3 Years" },
	{ id: 4, years: "3 to 4 Years" },
	{ id: 4, years: "4 to 5 Years" },
	{ id: 4, years: "5+ Years" },
]

function CheckedIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#fff" opacity="0.2" />
			<path
				d="M7 13l3 3 7-7"
				stroke="#fff"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	)
}
function CheckIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#2B4264" opacity="0.2" />
		</svg>
	)
}

const Step4 = ({ step, nextFormStep, prevFormStep }) => {
	const {
		register,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm()
	const { actions, state } = useStateMachine({ updateAction })

	const onSubmit = (data) => {
		actions.updateAction(data)
		nextFormStep()
	}
	const [modal, setModal] = useState(false)
	const [experience, setExperience] = useState(state.experience)
	const [selectedCountry, setSelectedCountry] = useState(state.highestEducation)
	const [selectedMasters, setSelectedMasters] = useState(state.highestEducation)
	const [percentage, setPercentage] = useState(state.percentage)

	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			className="w-full px-10 py-10 flex flex-col items-center space-y-6"
		>
			<h6 className="text-blue-900 font-extrabold text-2xl">
				Help Us build your Journey
			</h6>
			{state.lastQualification === "Bachelors" && (
				<Fragment>
					<h4 className="text-blue-900 text-3xl text-center">
						What is your highest education level?
					</h4>
					<div className="w-full px-4 py-10">
						<div className="mx-auto  lg:w-1/2 xl:w-2/3">
							<Controller
								control={control}
								defaultValue={state.highestEducation}
								name="highestEducation"
								rules={{ required: true }}
								render={({ onChange }) => (
									<RadioGroup
										value={selectedCountry}
										onChange={(e) => {
											onChange(e)
											setSelectedCountry(e)
										}}
									>
										<div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
											{countries.map((country) => (
												<RadioGroup.Option
													key={country.name}
													value={country.name}
													className={({ active, checked }) =>
														`${
															active
																? "ring-2 ring-white ring-opacity-60 ring-offset-2 ring-offset-sky-300"
																: ""
														}
                  ${
										checked
											? "bg-blue-900 bg-opacity-75 text-white"
											: "bg-gray-100"
									}
                    relative flex cursor-pointer rounded-lg px-5 py-4 focus:outline-none`
													}
												>
													{({ active, checked }) => (
														<>
															<div className="flex flex-col w-full items-center">
																<div className="flex w-full items-center justify-between gap-8">
																	<div className=" flex flex-col items-center justify-between gap-4">
																		<div
																			as="p"
																			className={`font-medium text-base whitespace-nowrap md:text-lg  ${
																				checked ? "text-white" : "text-gray-900"
																			}`}
																		>
																			{country.name}
																		</div>
																	</div>
																	{checked ? (
																		<div className="shrink-0 text-white ">
																			<CheckedIcon className="h-6 w-6" />
																		</div>
																	) : (
																		<div className="shrink-0">
																			<CheckIcon className="h-6 w-6" />
																		</div>
																	)}
																</div>
															</div>
														</>
													)}
												</RadioGroup.Option>
											))}
										</div>
									</RadioGroup>
								)}
							/>
						</div>
					</div>
					<h4 className="text-blue-900 text-3xl text-center">
						What is your expected or gained percentage ?
					</h4>
					<div className="bg-gray-100 rounded-lg px-4 py-4 flex items-center gap-3">
						<Controller
							control={control}
							defaultValue={state.percentage}
							name="percentage"
							rules={{ required: true, min: 33, max: 100 }}
							render={({ onChange }) => (
								<input
									type="number"
									value={percentage}
									onChange={(e) => {
										onChange(e)
										setPercentage(e.target.value)
									}}
									placeholder="Percentage"
									className="w-full h-full text-blue-900 bg-transparent appearance-none"
								/>
							)}
						/>
						<span className="text-xl text-blue-900">%</span>
					</div>
					{errors.percentage?.type === "required" && (
						<span className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Please fill all required fields
						</span>
					)}
					{errors.percentage?.type === "min" && (
						<span className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Percentage should be greater than 33
						</span>
					)}
					{errors.percentage?.type === "max" && (
						<span className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Percentage should not be greater than 100
						</span>
					)}
				</Fragment>
			)}
			{state.lastQualification === "Masters" && (
				<Fragment>
					<h4 className="text-blue-900 text-3xl text-center">
						What is your highest education level?
					</h4>
					<div className="w-full px-4 py-10">
						<div className="mx-auto  lg:w-1/2 xl:w-2/3">
							<Controller
								control={control}
								defaultValue={state.highestEducation}
								name="highestEducation"
								rules={{ required: true }}
								render={({ onChange }) => (
									<RadioGroup
										value={selectedMasters}
										onChange={(e) => {
											onChange(e)
											setSelectedMasters(e)
										}}
									>
										<div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
											{masters.map((country) => (
												<RadioGroup.Option
													key={country.name}
													value={country.name}
													className={({ active, checked }) =>
														`${
															active
																? "ring-2 ring-white ring-opacity-60 ring-offset-2 ring-offset-sky-300"
																: ""
														}
                  ${
										checked
											? "bg-blue-900 bg-opacity-75 text-white"
											: "bg-gray-100"
									}
                    relative flex cursor-pointer rounded-lg px-5 py-4 focus:outline-none`
													}
												>
													{({ active, checked }) => (
														<>
															<div className="flex flex-col w-full items-center">
																<div className="flex w-full items-center justify-between gap-8">
																	<div className=" flex flex-col items-center justify-between gap-4">
																		<div
																			as="p"
																			className={`font-medium text-base whitespace-nowrap md:text-lg  ${
																				checked ? "text-white" : "text-gray-900"
																			}`}
																		>
																			{country.name}
																		</div>
																	</div>
																	{checked ? (
																		<div className="shrink-0 text-white ">
																			<CheckedIcon className="h-6 w-6" />
																		</div>
																	) : (
																		<div className="shrink-0">
																			<CheckIcon className="h-6 w-6" />
																		</div>
																	)}
																</div>
															</div>
														</>
													)}
												</RadioGroup.Option>
											))}
										</div>
									</RadioGroup>
								)}
							/>
						</div>
					</div>
					<h4 className="text-blue-900 text-3xl text-center">
						What is your expected or gained percentage ?
					</h4>
					<div className="bg-gray-100 rounded-lg px-4 py-4 flex items-center gap-3">
						<Controller
							control={control}
							defaultValue={state.percentage}
							name="percentage"
							rules={{ required: true, min: 33, max: 100 }}
							render={({ onChange }) => (
								<input
									type="number"
									value={percentage}
									onChange={(e) => {
										onChange(e)
										setPercentage(e.target.value)
									}}
									placeholder="Percentage"
									className="w-full h-full text-blue-900 bg-transparent appearance-none"
								/>
							)}
						/>
						<span className="text-xl text-blue-900">%</span>
					</div>
					{errors.percentage?.type === "required" && (
						<span className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Please fill all required fields
						</span>
					)}
					{errors.percentage?.type === "min" && (
						<span className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Percentage should be greater than 33
						</span>
					)}
					{errors.percentage?.type === "max" && (
						<span className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Percentage should not be greater than 100
						</span>
					)}
				</Fragment>
			)}

			{state.lastQualification === "MBA" && (
				<Fragment>
					<h4 className="text-blue-900 text-3xl text-center">
						Do you have any work experience?
					</h4>
					<h4 className="text-blue-900 text-xl text-center">
						Having significant work experience increases your options multi
						fold.
					</h4>

					<div className="flex items-center gap-3">
						<span className="text-xl bg-blue-900 text-blue-100 px-12 py-3 rounded-full cursor-pointer">
							Yes
						</span>
						<span
							className="text-xl bg-gray-200 text-blue-900 px-12 py-3 rounded-full cursor-pointer"
							onClick={() => setModal(true)}
						>
							No
						</span>
					</div>
					<Transition appear show={modal} as={Fragment}>
						<Dialog
							as="div"
							className="relative z-10"
							onClose={() => setModal(false)}
						>
							<Transition.Child
								as={Fragment}
								enter="ease-out duration-300"
								enterFrom="opacity-0"
								enterTo="opacity-75"
								leave="ease-in duration-200"
								leaveFrom="opacity-75"
								leaveTo="opacity-0"
							>
								<div className="fixed inset-0 bg-blue-900 opacity-25" />
							</Transition.Child>

							<div className="fixed inset-0 overflow-y-auto">
								<div className="flex min-h-full items-center justify-center p-4 text-center">
									<Transition.Child
										as={Fragment}
										enter="ease-out duration-300"
										enterFrom="opacity-0 scale-95"
										enterTo="opacity-100 scale-100"
										leave="ease-in duration-200"
										leaveFrom="opacity-100 scale-100"
										leaveTo="opacity-0 scale-95"
									>
										<Dialog.Panel className="w-full max-w-4xl transform overflow-hidden rounded-lg bg-white p-6 text-left align-middle shadow-xl transition-all">
											<Dialog.Title
												as="h3"
												className="text-lg font-medium leading-6 text-gray-900"
											>
												Change Track
											</Dialog.Title>
											<div className="mt-2">
												<p className="text-sm text-gray-500">
													You don’t have significant work experience. With your
													current profile we recommened you to explore other
													masters programs. You are very likely to see very few
													universities and courses if you continue with the MBA
													track.
												</p>
											</div>

											<div className="mt-4">
												<button
													type="button"
													className="inline-flex justify-center rounded-md border border-transparent bg-blue-900 px-4 py-2 text-sm font-medium text-blue-100 hover:bg-transparent hover:border-blue-900 hover:text-blue-900"
													onClick={prevFormStep}
												>
													Explore Other Masters Program
												</button>
											</div>
										</Dialog.Panel>
									</Transition.Child>
								</div>
							</div>
						</Dialog>
					</Transition>
					<h4 className="text-blue-900 text-3xl text-center">
						How much work experience do you have?
					</h4>
					<div className="w-1/2 xl:w-2/3">
						<Controller
							control={control}
							defaultValue={state.experience}
							name="experience"
							rules={{ required: true }}
							render={({ onChange }) => (
								<Listbox
									value={experience}
									onChange={(e) => {
										onChange(e)
										setExperience(e)
									}}
								>
									<div className="relative mt-1 z-10">
										<Listbox.Button className="relative w-full cursor-default rounded-lg  py-3 pl-6 pr-10 text-left bg-gray-200 text-blue-900 focus:outline-none sm:text-lg ">
											{experience ? (
												<span className="block truncate">{experience}</span>
											) : (
												<span className="block truncate">
													Select Experience
												</span>
											)}
											<span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-6">
												<FiChevronDown
													className=" text-blue-900"
													aria-hidden="true"
												/>
											</span>
										</Listbox.Button>
										<Transition
											as={Fragment}
											leave="transition ease-in duration-100"
											leaveFrom="opacity-100"
											leaveTo="opacity-0"
										>
											<Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-base">
												{expYears.map((year, idx) => (
													<Listbox.Option
														key={idx}
														className={({ active }) =>
															`relative cursor-default select-none py-2 pl-6 pr-4 ${
																active
																	? "bg-blue-100 text-blue-900"
																	: "text-blue-900"
															}`
														}
														value={year.years}
													>
														{({ selected }) => (
															<>
																<span
																	className={`block truncate ${
																		selected ? "font-medium" : "font-normal"
																	}`}
																>
																	{year.years}
																</span>
																{selected ? (
																	<span className="absolute inset-y-0 right-0 flex items-center pr-6 text-blue-900">
																		<FiCheck aria-hidden="true" />
																	</span>
																) : null}
															</>
														)}
													</Listbox.Option>
												))}
											</Listbox.Options>
										</Transition>
									</div>
								</Listbox>
							)}
						/>
					</div>
				</Fragment>
			)}

			<div className="flex w-full lg:w-1/2 xl:w-4/6 items-center justify-between px-20 lg:px-4">
				<div
					onClick={prevFormStep}
					className="hover:bg-blue-900 border border-blue-900 hover:text-blue-100 text-blue-900 px-6 py-2 flex items-center justify-between gap-3 rounded-md cursor-pointer text-lg"
				>
					<BsArrowLeft />
					<span>Back</span>
				</div>
				<button
					type="submit"
					// onClick={nextFormStep}
					className="bg-blue-900 text-blue-100 px-6 py-2 rounded-md cursor-pointer text-lg flex items-center justify-between gap-3"
				>
					<span>Next</span>
					<BsArrowRight />
				</button>
			</div>
		</form>
	)
}

export default Step4
