import { useStateMachine } from "little-state-machine"
import Link from "next/link"
import React from "react"
import { MdVerified } from "react-icons/md"

const Step8 = () => {
	const { actions, state } = useStateMachine()

	return (
		<div className="w-full flex flex-col items-center py-8">
			<div className="w-4/5 lg:w-1/2 flex flex-col items-center bg-gray-200 px-10 py-8 space-y-4 rounded-lg">
				<MdVerified className="text-blue-600 text-4xl" />
				<p className="text-3xl font-bold text-blue-900">Thank You! </p>
				<p className="text-center text-blue-900 text-xl">
					Your Response is submitted successfully. Our team will contact you
					soon.
				</p>
				<Link href="/">
					<span className="text-blue-100 bg-blue-900 px-4 py-3 cursor-pointer rounded-lg">
						Go to Home
					</span>
				</Link>
			</div>
		</div>
	)
}

export default Step8
