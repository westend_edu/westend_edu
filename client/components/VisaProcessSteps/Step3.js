import React, { useState } from "react"
import { RadioGroup } from "@headlessui/react"
import { BsArrowLeft, BsArrowRight } from "react-icons/bs"
import { Controller, useForm } from "react-hook-form"
import { useStateMachine } from "little-state-machine"
import updateAction from "./action/updateAction"

const countries = [
	{ id: 1, name: "Bachelors" },
	{ id: 2, name: "Masters" },
	{ id: 3, name: "MBA" },
]

function CheckedIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#fff" opacity="0.2" />
			<path
				d="M7 13l3 3 7-7"
				stroke="#fff"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	)
}
function CheckIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#2B4264" opacity="0.2" />
		</svg>
	)
}

const Step3 = ({ step, nextFormStep, prevFormStep }) => {
	const {
		register,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm()
	const { actions, state } = useStateMachine({ updateAction })

	const onSubmit = (data) => {
		actions.updateAction(data)
		nextFormStep()
	}
	const [selectedCountry, setSelectedCountry] = useState(
		state.lastQualification
	)
	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			className="w-full px-10 py-10 flex flex-col items-center space-y-6"
		>
			<h6 className="text-blue-900 font-extrabold text-2xl">
				Help Us build your Journey
			</h6>
			<h4 className="text-blue-900 text-3xl text-center">
				What is your last qualification?
			</h4>

			<div className="w-full px-4 py-10">
				<div className="mx-auto flex flex-col space-y-4 lg:w-1/2 xl:w-2/3">
					<Controller
						control={control}
						defaultValue={state.lastQualification}
						name="lastQualification"
						rules={{ required: true }}
						render={({ onChange }) => (
							<RadioGroup
								value={selectedCountry}
								onChange={(e) => {
									onChange(e)
									setSelectedCountry(e)
								}}
							>
								<div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4">
									{countries.map((country) => (
										<RadioGroup.Option
											key={country.name}
											value={country.name}
											className={({ active, checked }) =>
												`${
													active
														? "ring-2 ring-white ring-opacity-60 ring-offset-2 ring-offset-sky-300"
														: ""
												}
                  ${
										checked
											? "bg-blue-900 bg-opacity-75 text-white"
											: "bg-gray-100"
									}
                    relative flex cursor-pointer rounded-lg px-5 py-4 focus:outline-none`
											}
										>
											{({ active, checked }) => (
												<>
													<div className="flex flex-col w-full items-center">
														<div className="flex w-full items-center justify-between gap-8">
															<div className=" flex flex-col items-center justify-between gap-4">
																<div
																	as="p"
																	className={`font-medium text-base whitespace-nowrap md:text-lg  ${
																		checked ? "text-white" : "text-gray-900"
																	}`}
																>
																	{country.name}
																</div>
															</div>
															{checked ? (
																<div className="shrink-0 text-white ">
																	<CheckedIcon className="h-6 w-6" />
																</div>
															) : (
																<div className="shrink-0">
																	<CheckIcon className="h-6 w-6" />
																</div>
															)}
														</div>
													</div>
												</>
											)}
										</RadioGroup.Option>
									))}
								</div>
							</RadioGroup>
						)}
					/>
					{errors.lastQualification?.type === "required" && (
						<div className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Please select a Last Qualification
						</div>
					)}
				</div>
			</div>

			<div className="flex w-full lg:w-1/2 xl:w-4/6 items-center justify-between px-20 lg:px-4">
				<div
					onClick={prevFormStep}
					className="hover:bg-blue-900 border border-blue-900 hover:text-blue-100 text-blue-900 px-6 py-2 flex items-center justify-between gap-3 rounded-md cursor-pointer text-lg"
				>
					<BsArrowLeft />
					<span>Back</span>
				</div>
				<button
					type="submit"
					// onClick={nextFormStep}
					className="bg-blue-900 text-blue-100 px-6 py-2 rounded-md cursor-pointer text-lg flex items-center justify-between gap-3"
				>
					<span>Next</span>
					<BsArrowRight />
				</button>
			</div>
		</form>
	)
}

export default Step3
