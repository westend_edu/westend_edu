import React, { useState } from "react"
import { RadioGroup } from "@headlessui/react"
import { BsArrowLeft, BsArrowRight } from "react-icons/bs"
import { Controller, useForm } from "react-hook-form"
import { useStateMachine } from "little-state-machine"
import updateAction from "./action/updateAction"

const countries = [
	{ id: 1, name: "USA", flag: "/images/countries/usa.svg" },
	{ id: 2, name: "UK", flag: "/images/countries/uk.svg" },
	{ id: 3, name: "Canada", flag: "/images/countries/canada.svg" },
	{ id: 4, name: "Australia", flag: "/images/countries/australia.svg" },
	{ id: 5, name: "Ireland", flag: "/images/countries/ireland.svg" },
	{ id: 6, name: "Open to All" },
]

function CheckedIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#fff" opacity="0.2" />
			<path
				d="M7 13l3 3 7-7"
				stroke="#fff"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	)
}
function CheckIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#2B4264" opacity="0.2" />
		</svg>
	)
}

const Step1 = ({ step, nextFormStep, prevFormStep }) => {
	const {
		register,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm()
	const { actions, state } = useStateMachine({ updateAction })

	const onSubmit = (data) => {
		actions.updateAction(data)
		nextFormStep()
	}

	const [selectedCountry, setSelectedCountry] = useState(state.country)
	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			className="w-full px-10 py-10 flex flex-col items-center space-y-6"
		>
			<h6 className="text-blue-900 font-extrabold text-2xl">
				Help Us build your Journey
			</h6>
			<h4 className="text-blue-900 text-3xl text-center">
				What is your desire country to to pursue your education in?
			</h4>
			<div className="w-full px-4 py-6">
				<div className="mx-auto flex flex-col space-y-4 lg:w-4/5 xl:w-2/3">
					<Controller
						control={control}
						defaultValue={state.country}
						name="country"
						rules={{ required: true }}
						render={({ onChange }) => (
							<RadioGroup
								value={selectedCountry}
								onChange={(e) => {
									onChange(e)
									setSelectedCountry(e)
								}}
							>
								<div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4">
									{countries.map((country) => (
										<RadioGroup.Option
											key={country.name}
											value={country.name}
											className={({ active, checked }) =>
												`${
													active
														? "ring-2 ring-white ring-opacity-60 ring-offset-2 ring-offset-sky-300"
														: ""
												}
                  ${
										checked
											? "bg-blue-900 bg-opacity-75 text-white"
											: "bg-gray-100"
									}
                    relative flex cursor-pointer rounded-lg px-5 py-4 focus:outline-none`
											}
										>
											{({ active, checked }) => (
												<>
													<div className="flex flex-col w-full items-center">
														<div className="flex w-full items-start justify-between gap-8">
															<div className=" flex flex-col items-center justify-between gap-4">
																{country.flag ? (
																	<div
																		as="span"
																		className={`inline ${
																			checked ? "text-sky-100" : "text-gray-500"
																		}`}
																	>
																		<img
																			src={country.flag}
																			className="w-11 h-10"
																		/>
																	</div>
																) : null}
																<div
																	as="p"
																	className={`font-medium text-base whitespace-nowrap md:text-lg  ${
																		checked ? "text-white" : "text-gray-900"
																	}`}
																>
																	{country.name}
																</div>
															</div>
															{checked ? (
																<div className="shrink-0 text-white ">
																	<CheckedIcon className="h-6 w-6" />
																</div>
															) : (
																<div className="shrink-0">
																	<CheckIcon className="h-6 w-6" />
																</div>
															)}
														</div>
													</div>
												</>
											)}
										</RadioGroup.Option>
									))}
								</div>
							</RadioGroup>
						)}
					/>

					{errors.country?.type === "required" && (
						<span className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Please select a Country
						</span>
					)}
				</div>
			</div>

			<div className="flex w-full lg:w-1/2 xl:w-4/6 items-center justify-end px-20 lg:px-4">
				<button
					type="submit"
					// onClick={nextFormStep}
					className="bg-blue-900 text-blue-100 px-6 py-2 rounded-md cursor-pointer text-lg flex items-center justify-between gap-3"
				>
					<span>Next</span>
					<BsArrowRight />
				</button>
			</div>
		</form>
	)
}

export default Step1
