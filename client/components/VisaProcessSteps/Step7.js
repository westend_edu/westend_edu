import React, { useState } from "react"
import { RadioGroup } from "@headlessui/react"
import { FiSearch } from "react-icons/fi"
import { BsArrowLeft, BsArrowRight } from "react-icons/bs"
import { useStateMachine } from "little-state-machine"
import { Controller, useForm } from "react-hook-form"
import updateAction from "./action/updateAction"
import Link from "next/link"
import axios from "axios"
import baseUrl from "@/utils/baseUrl"

const countries = [
	{ id: 1, name: "IELTS" },
	{ id: 2, name: "TOFEL" },
	{ id: 3, name: "PTE" },
	{ id: 4, name: "Not Planning To Take Any" },
]

function CheckedIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#fff" opacity="0.2" />
			<path
				d="M7 13l3 3 7-7"
				stroke="#fff"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	)
}
function CheckIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#2B4264" opacity="0.2" />
		</svg>
	)
}

const Step7 = ({ step, nextFormStep, prevFormStep }) => {
	const {
		register,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm()
	const { actions, state } = useStateMachine({ updateAction })

	const onSubmit = async (data) => {
		await actions.updateAction(data)

		axios
			.post(`${baseUrl}/visa/createVisaDetails`, {
				Name: data.fullName,
				MobileNumber: data.mobileNo,
				EmailId: data.email,
				City: data.city,
				Country: state.country,
				PreferredIntake: state.preferedIntake,
				LastQualification: state.lastQualification,
				HighestQualification: state.highestEducation,
				Percentage: state.percentage,
				YearsOfExperience: state.experience,
				DesiredField: state.desiredFields,
				EnglishTest: state.test,
				EnglishTestScore: state.testScore,
			})
			.then((response) => {
				nextFormStep()
			})
			.catch((error) => {
				console.log(error)
			})
	}

	const [fullName, setFullName] = useState(state.fullName)
	const [mobileNo, setMobileNo] = useState(state.mobileNo)
	const [email, setEmail] = useState(state.email)
	const [city, setCity] = useState(state.city)

	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			className="w-full px-10 py-10 flex flex-col items-center space-y-6"
		>
			<h6 className="text-blue-900 font-extrabold text-2xl">
				Help Us build your Journey
			</h6>
			<h4 className="text-blue-900 text-3xl text-center">Enter Your Details</h4>
			<div className="w-full flex flex-col space-y-4 px-4 items-center">
				<div className="w-full lg:w-4/6 flex flex-col items-start gap-2">
					<Controller
						control={control}
						defaultValue={state.fullName}
						name="fullName"
						rules={{
							required: true,
							minLength: 3,
							maxLength: 20,
							pattern: /^[^\s]+(?:$|.*[^\s]+$)/,
						}}
						render={({ onChange }) => (
							<input
								type="text"
								placeholder="Full Name"
								value={fullName}
								onChange={(e) => {
									onChange(e)
									setFullName(e.target.value)
								}}
								className="w-full rounded-lg bg-gray-100 h-full px-4 py-4 text-blue-900"
							/>
						)}
					/>
					<span className="text-sm text-red-500">
						{errors.fullName?.type === "required" && "Name is required"}
						{errors.fullName?.type === "minLength" &&
							"Name should be atleast 3 characters."}
						{errors.fullName?.type === "maxLength" &&
							"Name should not be greater than 20."}
						{errors.fullName?.type === "pattern" &&
							"Please remove extra white spaces."}
					</span>
				</div>
				<div className="w-full lg:w-4/6 flex flex-col items-start gap-2">
					<Controller
						control={control}
						defaultValue={state.mobileNo}
						name="mobileNo"
						rules={{ required: true, minLength: 10, maxLength: 10 }}
						render={({ onChange }) => (
							<input
								type="number"
								placeholder="Mobile No."
								value={mobileNo}
								onChange={(e) => {
									onChange(e)
									setMobileNo(e.target.value)
								}}
								className="w-full rounded-lg bg-gray-100 h-full px-4 py-4 text-blue-900"
							/>
						)}
					/>
					<span className="text-sm text-red-500">
						{errors.mobileNo?.type === "required" && "Mobile is required"}
						{errors.mobileNo?.type === "minLength" &&
							"Mobile Number should be 10 Digits."}
						{errors.mobileNo?.type === "maxLength" &&
							"Mobile Number should be 10 Digits."}
					</span>
				</div>
				<div className="w-full lg:w-4/6 flex flex-col items-start gap-2">
					<Controller
						control={control}
						defaultValue={state.email}
						name="email"
						rules={{ required: true, pattern: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/ }}
						render={({ onChange }) => (
							<input
								type="text"
								placeholder="Email"
								value={email}
								onChange={(e) => {
									onChange(e)
									setEmail(e.target.value)
								}}
								className="w-full rounded-lg bg-gray-100 h-full px-4 py-4 text-blue-900"
							/>
						)}
					/>
					<span className="text-sm text-red-500">
						{errors.email?.type === "required" && "Email is required"}
						{errors.email?.type === "pattern" && "Please enter valid Email"}
					</span>
				</div>
				<div className="w-full lg:w-4/6 flex flex-col items-start gap-2">
					<Controller
						control={control}
						defaultValue={state.city}
						name="city"
						rules={{ required: true, pattern: /^[^\s]+(?:$|.*[^\s]+$)/ }}
						render={({ onChange }) => (
							<input
								type="text"
								placeholder="Your City"
								value={city}
								onChange={(e) => {
									onChange(e)
									setCity(e.target.value)
								}}
								className="w-full rounded-lg bg-gray-100 h-full px-4 py-4 text-blue-900"
							/>
						)}
					/>
					<span className="text-sm text-red-500">
						{errors.city?.type === "required" && "City is required"}
						{errors.city?.type === "pattern" && "Please Rxemove Extra Spaces."}
					</span>
				</div>
			</div>

			<div className="flex w-full lg:w-4/6 items-center justify-between px-20 lg:px-4">
				<div
					onClick={prevFormStep}
					className="hover:bg-blue-900 border border-blue-900 hover:text-blue-100 text-blue-900 px-6 py-2 flex items-center justify-between gap-3 rounded-md cursor-pointer text-lg"
				>
					<BsArrowLeft />
					<span>Back</span>
				</div>
				<button
					type="submit"
					// onClick={nextFormStep}
					className="bg-blue-900 text-blue-100 px-6 py-2 rounded-md cursor-pointer text-lg flex items-center justify-between gap-3"
				>
					<span>Submit</span>
					<BsArrowRight />
				</button>
			</div>
		</form>
	)
}

export default Step7
