import React, { useEffect, useState } from "react"
import { RadioGroup } from "@headlessui/react"
import { FiSearch } from "react-icons/fi"
import { BsArrowLeft, BsArrowRight } from "react-icons/bs"
import { useStateMachine } from "little-state-machine"
import { Controller, useForm } from "react-hook-form"
import updateAction from "./action/updateAction"
import Link from "next/link"
import axios from "axios"
import baseUrl from "@/utils/baseUrl"

const countries = [
	{ id: 1, name: "IELTS" },
	{ id: 2, name: "TOFEL" },
	{ id: 3, name: "PTE" },
	{ id: 4, name: "Not Planning To Take Any" },
]

function CheckedIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#fff" opacity="0.2" />
			<path
				d="M7 13l3 3 7-7"
				stroke="#fff"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	)
}
function CheckIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#2B4264" opacity="0.2" />
		</svg>
	)
}

const Step6 = ({ step, nextFormStep, prevFormStep }) => {
	const {
		register,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm()
	const { actions, state } = useStateMachine({ updateAction })

	const onSubmit = (data) => {
		actions.updateAction(data)
		nextFormStep()
	}
	const [selectedCountry, setSelectedCountry] = useState(state.test)
	const [testScore, setTestScore] = useState(state.testScore)
	const [testScoreInput, setTestScoreInput] = useState(false)

	useEffect(() => {
		selectedCountry === "Not Planning To Take Any" ||
		selectedCountry === undefined
			? setTestScoreInput(false)
			: setTestScoreInput(true)
	}, [testScoreInput, selectedCountry])

	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			className="w-full px-10 py-10 flex flex-col items-center space-y-6"
		>
			<h6 className="text-blue-900 font-extrabold text-2xl">
				Help Us build your Journey
			</h6>
			<h4 className="text-blue-900 text-3xl text-center">
				Which english language test have you taken OR are planning to take?
			</h4>
			<h6 className="text-blue-900 text-xl text-center">
				Scoring high in language tests increases your options multi fold.
			</h6>

			<div className="w-full px-4 py-10">
				<div className="mx-auto flex flex-col space-y-4 lg:w-3/4 xl:w-2/3">
					<Controller
						control={control}
						defaultValue={state.test}
						name="test"
						rules={{ required: true }}
						render={({ onChange }) => (
							<RadioGroup
								value={selectedCountry}
								onChange={(e) => {
									onChange(e)
									setSelectedCountry(e)
								}}
							>
								<div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
									{countries.map((country) => (
										<RadioGroup.Option
											key={country.name}
											value={country.name}
											className={({ active, checked }) =>
												`${
													active
														? "ring-2 ring-white ring-opacity-60 ring-offset-2 ring-offset-sky-300"
														: ""
												}
                  ${
										checked
											? "bg-blue-900 bg-opacity-75 text-white"
											: "bg-gray-100"
									}
                    relative flex cursor-pointer rounded-lg px-5 py-4 focus:outline-none`
											}
										>
											{({ active, checked }) => (
												<>
													<div className="flex flex-col w-full items-center">
														<div className="flex w-full items-center justify-between gap-8">
															<div className=" flex flex-col items-center justify-between gap-4">
																<div
																	as="p"
																	className={`font-medium text-base whitespace-nowrap md:text-lg  ${
																		checked ? "text-white" : "text-gray-900"
																	}`}
																>
																	{country.name}
																</div>
															</div>
															{checked ? (
																<div className="shrink-0 text-white ">
																	<CheckedIcon className="h-6 w-6" />
																</div>
															) : (
																<div className="shrink-0">
																	<CheckIcon className="h-6 w-6" />
																</div>
															)}
														</div>
													</div>
												</>
											)}
										</RadioGroup.Option>
									))}
									{/* <Link href="/signup">
										<div className="bg-gray-100 flex items-center justify-between w-full h-full cursor-pointer rounded-lg px-5 py-4 focus:outline-none">
											<span className="font-medium text-base whitespace-nowrap md:text-lg">
												Not Planning To Take Any
											</span>
										</div>
									</Link> */}
								</div>
							</RadioGroup>
						)}
					/>
					{errors.test?.type === "required" && (
						<div className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Please select a test.
						</div>
					)}
				</div>
			</div>
			{testScoreInput === true ? (
				<>
					<h4 className="text-blue-900 text-3xl text-center">
						Enter Your Score
					</h4>
					<div className="bg-gray-100 rounded-lg px-4 py-4 flex items-center gap-3">
						<Controller
							control={control}
							defaultValue={state.testScore}
							name="testScore"
							rules={{
								required: true,
								min:
									selectedCountry === "IELTS"
										? 0
										: selectedCountry === "TOFEL"
										? 0
										: selectedCountry === "PTE"
										? 10
										: 0,
								max:
									selectedCountry === "IELTS"
										? 9
										: selectedCountry === "TOFEL"
										? 120
										: selectedCountry === "PTE"
										? 90
										: 0,
							}}
							render={({ onChange }) => (
								<input
									type="number"
									placeholder="Score"
									value={testScore}
									onChange={(e) => {
										onChange(e)
										setTestScore(e.target.value)
									}}
									className="w-full h-full text-blue-900 bg-transparent"
								/>
							)}
						/>
					</div>
					{errors.testScore?.type === "required" && (
						<div className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							Please submit your test score.
						</div>
					)}
					{errors.testScore?.type === "min" && (
						<div className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							{selectedCountry === "IELTS"
								? "IELTS Score must be in between 0 - 9"
								: selectedCountry === "TOFEL"
								? "TOFEL Score must be in between 0 - 120"
								: selectedCountry === "PTE"
								? "PTE Score must be in between 10 - 90"
								: "Please Fill Correct Test Score."}
						</div>
					)}
					{errors.testScore?.type === "max" && (
						<div className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
							{selectedCountry === "IELTS"
								? "IELTS Score must be in between 0 - 9"
								: selectedCountry === "TOFEL"
								? "TOFEL Score must be in between 0 - 120"
								: selectedCountry === "PTE"
								? "PTE Score must be in between 10 - 90"
								: "Please Fill Correct Test Score."}
						</div>
					)}
				</>
			) : null}

			<div className="flex w-full lg:w-1/2 xl:w-4/6 items-center justify-between px-20 lg:px-4">
				<div
					onClick={prevFormStep}
					className="hover:bg-blue-900 border border-blue-900 hover:text-blue-100 text-blue-900 px-6 py-2 flex items-center justify-between gap-3 rounded-md cursor-pointer text-lg"
				>
					<BsArrowLeft />
					<span>Back</span>
				</div>
				<button
					type="submit"
					// onClick={nextFormStep}
					className="bg-blue-900 text-blue-100 px-6 py-2 rounded-md cursor-pointer text-lg flex items-center justify-between gap-3"
				>
					<span>Next</span>
					<BsArrowRight />
				</button>
			</div>
		</form>
	)
}

export default Step6
