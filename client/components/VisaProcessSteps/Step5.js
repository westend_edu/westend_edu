import { useStateMachine } from "little-state-machine"
import React, { Fragment, useState } from "react"
import { Controller, useForm } from "react-hook-form"
import { BsArrowLeft, BsArrowRight } from "react-icons/bs"
import Select from "react-select"
import makeAnimated from "react-select/animated"
import updateAction from "./action/updateAction"

const animatedComponents = makeAnimated()

const countries = [
	{ label: "Management", value: "Management" },
	{ label: "Engineering", value: "Engineering" },
	{ label: "Computer & Data Science", value: "Computer & Data Science" },
	{ label: "Design", value: "Design" },
	{ label: "Finance & Banking", value: "Finance & Banking" },
	{ label: "Law", value: "Law" },
	{
		label: "Humanities and Social Sciences",
		value: "Humanities and Social Sciences",
	},
	{ label: "Sciences", value: "Sciences" },
	{ label: "Medicine and Pharma", value: "Medicine and Pharma" },
	{
		label: "Performing and Creative Arts",
		value: "Performing and Creative Arts",
	},
	{ label: "Media and Journalism", value: "Media and Journalism" },
	{ label: "Hospitality and Tourism", value: "Hospitality and Tourism" },
	{ label: "Marketing and Advertising", value: "Marketing and Advertising" },
	{ label: "Sports and Nutrition", value: "Sports and Nutrition" },
	{ label: "Architecture", value: "Architecture" },
	{ label: "Other", value: "Other" },
]

function CheckedIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#fff" opacity="0.2" />
			<path
				d="M7 13l3 3 7-7"
				stroke="#fff"
				strokeWidth={1.5}
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	)
}
function CheckIcon(props) {
	return (
		<svg viewBox="0 0 24 24" fill="none" {...props}>
			<circle cx={12} cy={12} r={12} fill="#2B4264" opacity="0.2" />
		</svg>
	)
}

const Step5 = ({ step, nextFormStep, prevFormStep }) => {
	const {
		register,
		control,
		handleSubmit,
		formState: { errors },
	} = useForm()
	const { actions, state } = useStateMachine({ updateAction })

	const onSubmit = (data) => {
		actions.updateAction(data)
		nextFormStep()
	}
	const [selectedCountry, setSelectedCountry] = useState(state.desiredFields)
	const [query, setQuery] = useState("")
	return (
		<form
			onSubmit={handleSubmit(onSubmit)}
			className="w-full px-10 py-10 flex flex-col items-center space-y-6"
		>
			<h6 className="text-blue-900 font-extrabold text-2xl">
				Help Us build your Journey
			</h6>
			<h4 className="text-blue-900 text-3xl text-center">
				What is your desired field do you wish to pursue?
			</h4>

			<div className="w-full flex flex-col space-y-4 xl:w-2/3 px-4 z-20">
				<Controller
					control={control}
					defaultValue={state.desiredFields}
					name="desiredFields"
					rules={{ required: true }}
					render={({ onChange }) => (
						<Select
							closeMenuOnSelect={false}
							components={animatedComponents}
							defaultValue={state.desiredFields}
							isMulti
							onChange={(e) => {
								onChange(e)
								setSelectedCountry(e)
							}}
							options={countries}
						/>
					)}
				/>
				{errors.desiredFields?.type === "required" && (
					<div className="text-red-600 font-bold text-lg bg-red-200 px-3 py-2 rounded-lg text-center">
						Please select at least one field.
					</div>
				)}
			</div>
			<div className="flex w-full lg:w-1/2 xl:w-4/6 items-center justify-between px-20 lg:px-4">
				<div
					onClick={prevFormStep}
					className="hover:bg-blue-900 border border-blue-900 hover:text-blue-100 text-blue-900 px-6 py-2 flex items-center justify-between gap-3 rounded-md cursor-pointer text-lg"
				>
					<BsArrowLeft />
					<span>Back</span>
				</div>
				<button
					type="submit"
					// onClick={nextFormStep}
					className="bg-blue-900 text-blue-100 px-6 py-2 rounded-md cursor-pointer text-lg flex items-center justify-between gap-3"
				>
					<span>Next</span>
					<BsArrowRight />
				</button>
			</div>
		</form>
	)
}

export default Step5
