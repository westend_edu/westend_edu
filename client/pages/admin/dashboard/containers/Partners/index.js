import React from "react";
import CreateModal from "../../../../../components/Modals";
import EditModal from "../../../../../components/Modals";
import { PartnerListheadCells } from "../../../../../utils/Globals/index.js";
import Table2 from "../../../../../components/Table/partner.js";
import Notifier from "../../../../../utils/Notifier";
import { Store } from "../../../../../Context";
import baseUrl from "../../../../../utils/baseUrl";
import Axios from "axios";
import CreatePartner from "./Create/create";
import EditPartner from "./Edit/index";
import Filter from "../filter";
import { getUser, getToken } from "../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function listOfPartners(props) {
  const [studentId, setstudentId] = React.useState(false);
  const [createModal, setcreateModal] = React.useState(false);
  const [partnerSelected, setpartnerSelected] = React.useState(false);

  const [partnersArray, setpartnersArray] = React.useState([]);

  const createToggle = () => {
    setcreateModal(!createModal);
  };
  const [editModal, seteditModal] = React.useState(false);
  const editToggle = (id) => {
    if (id) {
      setstudentId(id);
    }
    seteditModal(!editModal);
  };

  const handleDelete = async (id) => {
    const confirmis = window.confirm("Are you sure you want to delete?");

    if (confirmis === true) {
      let fill = props.data.filter((dt) => dt.id !== id);
      props.setData(fill);
      try {
        let response = await Axios({
          method: "delete",
          url: `${baseUrl}/admin/deleteUser/${id}`,

          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        Notifier(response.data.message, "success");
        // router.reload();
      } catch (err) {
        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    }
  };
  const onCheckBoxChange = (id) => {
    setpartnerSelected(false);
    setpartnersArray([...partnersArray, id]);
  };
  const handleApprove = async () => {
    if (partnersArray && partnersArray.length > 0) {
      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/admin/approvePartner`,
          data: partnersArray,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        props.setData(response.data.data);
        setpartnerSelected(true);
        Notifier(response.data.message, "success");
        setpartnersArray([]);
        // router.reload();
      } catch (err) {
        setpartnerSelected(false);

        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    } else {
      Notifier("Please select the partner for approval", "error");
    }
  };
  return (
    <div>
      <CreateModal
        title="Create Partner"
        size="xl"
        modal={createModal}
        toggle={createToggle}
        setData={props.setData}
        content={
          <CreatePartner
            createModal={createModal}
            toggle={createToggle}
            setData={props.setData}
          />
        }
      />
      <EditModal
        title="Edit Partner"
        size="xl"
        modal={editModal}
        toggle={editToggle}
        content={
          <EditPartner
            data={props.data}
            setData={props.setData}
            id={studentId}
            toggle={editToggle}
          />
        }
      />
      <Filter
        setData={props.setData}
        createToggle={createToggle}
        data={props.data}
        filter="PARTNER"
        activeTab={props.activeTab}
        tableID="partnerID"
        partnersArray={partnersArray}
        handleApprove={handleApprove}
      />
      {props.data && props.data.length > 0 ? (
        <Table2
          id="partnerID"
          data={props.data}
          handleEdit={editToggle}
          handleDelete={handleDelete}
          headCells={PartnerListheadCells}
          onCheckBoxChange={onCheckBoxChange}
          partnerSelected={partnerSelected}
          handleSwitch={props.handleSwitchPartner}
        />
      ) : (
        <h6 className="text-center">No Data Found</h6>
      )}
    </div>
  );
}
