import React, { useEffect } from "react"
import Axios from "axios"
import Notifier from "../../../../../../utils/Notifier"
import baseUrl from "../../../../../../utils/baseUrl"
import AutoComplete from "../../../../../../components/AutoComplete/Index.js"
import { application_status } from "../../../../../../utils/Globals"
import { Store } from "../../../../../../Context"
import PhoneInput from "../../../../../../components/CustomPhoneInput.js"

import { useFormik } from "formik"
import * as Yup from "yup"
import { getUser, getToken } from "../../../../../../utils/auth"
const token = getToken()
const loggedInUser = getUser()
export default function Create(props) {
	const [loading, setloading] = React.useState(false)

	const formik = useFormik({
		initialValues: {
			comment: "",
		},
		validationSchema: Detail_YUP,
		onSubmit: async (values) => {
			setloading(true)
			try {
				let response = await Axios({
					method: "post",
					url: `${baseUrl}/admin/addVisaComment`,
					data: {
						comment: values?.comment,
						visaId: props?.id,
					},
					headers: {
						Authorization: `Bearer ${token}`,
					},
				})
				props.toggle()
				setloading(false)

				Notifier(response.data.message, "success")

				// router.push("/profile");
			} catch (err) {
				setloading(false)
				if (err.response) {
					Notifier(err.response.data.message, "error")
				}
			}
		},
	})
	const handleChange = (e) => {
		const { name, value } = e.target
		if (name === "phone") {
			formik.setFieldValue(name, value.replace(/\D/g, ""))
		} else {
			formik.setFieldValue(name, value)
		}
	}

	return (
		<div>
			<form className="w-full">
				<div className="flex no-wrap -mx-3 mb-3">
					<div className="w-full px-3 mb-6 md:mb-0">
						<label
							className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
							htmlFor="grid-first-name"
						>
							Comment
						</label>

						<textarea
							id="noter-text-area"
							placeholder="Comments"
							className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
							name="comment"
							onChange={handleChange}
							value={formik.values.comment}
						/>
					</div>
				</div>
				<div className="text-danger ">
					{formik.touched.comment && formik.errors.comment ? (
						<div>{formik.errors.comment}</div>
					) : null}
				</div>
				<div className="row pb-2 ">
					<div className="col-lg-12 text-right">
						<button
							// disabled={activeStep === 0}
							onClick={formik.handleSubmit}
							className="default-btn4"
						>
							{loading ? "Adding..." : "Add"} <span></span>
						</button>
					</div>
				</div>
			</form>
		</div>
	)
}

const Detail_YUP = Yup.object({
	comment: Yup.string()
		.min(2, "Must 2 characters long")
		.required("Required")
		.matches(/^[^\s].([A-Za-z]+\s)*[A-Za-z]+$/, "Extra Space Not Allowed"),
})
