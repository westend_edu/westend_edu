import React, { useEffect } from "react"
import Axios from "axios"
import Notifier from "../../../../../../utils/Notifier"
import baseUrl from "../../../../../../utils/baseUrl"
import AutoComplete from "../../../../../../components/AutoComplete/Index.js"
import { application_status } from "../../../../../../utils/Globals"
import { Store } from "../../../../../../Context"
import PhoneInput from "../../../../../../components/CustomPhoneInput.js"

import { useFormik } from "formik"
import * as Yup from "yup"
import { getUser, getToken } from "../../../../../../utils/auth"
import axios from "axios"
const token = getToken()
const loggedInUser = getUser()
export default function Create(props) {
	const [loading, setloading] = React.useState(false)
	const [visaData, setVisaData] = React.useState({})
	useEffect(() => {
		props?.id &&
			axios
				.get(`${baseUrl}/visa/getVisaDetails/${props?.id}`)
				.then((response) => {
					console.log("response", response.data?.data)
					setVisaData(response.data?.data)
				})
				.catch((error) => {})
	}, [props])

	return (
		<div>
			<div className="flex flex-col px-8 space-y-3">
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Name:</span>
					<span>{visaData?.name}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Email:</span>
					<span>{visaData?.emailId}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Mobile No:</span>
					<span>{visaData?.mobileNumber}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Country:</span>
					<span>{visaData?.country}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Preferred Intake:</span>
					<span>{visaData?.preferredIntake}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Last Qualification:</span>
					<span>{visaData?.lastQualification}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">highest Qualification:</span>
					<span>{visaData?.highestQualification}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Percentage:</span>
					<span>{visaData?.percentage}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Years Of Experience:</span>
					<span>{visaData?.yearsOfExperience}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">Desired Fields:</span>
					<span>
						{visaData?.desiredField?.map((field) => (
							<span>{field.value}, </span>
						))}
					</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">English Test:</span>
					<span>{visaData?.englishTest}</span>
				</div>
				<div className="flex items-center space-x-3 text-base">
					<span className="font-bold">English Test Score:</span>
					<span>{visaData?.englishTestScore}</span>
				</div>
				<div className="flex space-x-3 text-base">
					<span className="font-bold">Comments:</span>
					<div className="flex flex-col space-y-3">
						{visaData?.comments?.map((comment) => (
							<div className="flex flex-col space-y-1">
								<span className="font-semibold">By {comment?.commentBy}</span>
								<span className="text-sm">{comment?.comment}</span>
							</div>
						))}
					</div>
				</div>
			</div>
		</div>
	)
}
