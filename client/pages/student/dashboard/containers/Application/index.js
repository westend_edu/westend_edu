import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepButton from "@material-ui/core/StepButton";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import CourseDetails from "./components/CourseDetails/index.js";
import BasicDetails from "./components/BasicDetails/index.js";
import TestScores from "./components/TestScores/index.js";
import CoApplicant from "./components/Co-Apllicant/index.js";
import CoApplicant2 from "./components/Co-Apllicant2/index.js";
import EducationDetails from "./components/EducationDetails/index.js";
import UploadDocuments from "./components/UploadDocuments/index.js";

import { parseCookies } from "nookies";
import { Store } from "../../../../../Context";
import Axios from "axios";
import baseUrl from "../../../../../utils/baseUrl";
import cookie from "js-cookie";
import { getUser, getToken } from "../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    // background: "red",
    border: "1px",
    borderColor: "#c4c4c4",
    border: "0.1px solid #c4c4c4",
    padding: "2%",
    ["@media (max-width:450px)"]: {
      padding: "4% 2% 10% 3% ",
    },
  },
  button: {
    marginRight: theme.spacing(1),
    border: "none",
    position: "relative",
    display: "inline - block",
    textAlign: "center",
    overflow: "hidden",
    zIndex: 1,
    color: "#fff",
    backgroundColor: "#eb4034",
    transition: "0.5s",
    borderRadius: " 5px",
    fontWeight: 700,
    fontSize: "16px",
    paddingLeft: "40px",
    paddingRight: "40px",
    paddingTop: "7px",
    paddingBottom: "7px",
  },
  button2: {
    marginRight: theme.spacing(1),
    border: "none",
    position: "relative",
    display: "inline - block",
    textAlign: "center",
    overflow: "hidden",
    zIndex: 1,
    color: "#fff",
    backgroundColor: "#152f68",
    transition: "0.5s",
    borderRadius: " 5px",
    fontWeight: 700,
    fontSize: "16px",
    paddingLeft: "40px",
    paddingRight: "40px",
    paddingTop: "7px",
    paddingBottom: "7px",
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  completed: {
    display: "inline-block",
  },
  instructions: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return [
    "Basic Details",
    "Course Details",
    "Education Details",
    "Test Scores",
    "Co-Applicant 1's Details",
    "Co-Applicant 2's Details",
    "Upload Documents",
  ];
}

function getStepContent(
  step,
  handleBack,
  handleNext,
  completedSteps,
  totalSteps,
  activeStep,
  steps,
  handleSkip,
  data,
  studentID,
  setData
) {
  switch (step) {
    case 0:
      return (
        <BasicDetails
          handleBack={handleBack}
          handleNext={handleNext}
          completedSteps={completedSteps}
          totalSteps={totalSteps}
          activeStep={activeStep}
          steps={steps}
          data={data}
          handleSkip={handleSkip}
          studentID={studentID}
          setData={setData}
        />
      );
    case 1:
      return (
        <CourseDetails
          handleBack={handleBack}
          handleNext={handleNext}
          completedSteps={completedSteps}
          totalSteps={totalSteps}
          activeStep={activeStep}
          steps={steps}
          data={data}
          handleSkip={handleSkip}
          studentID={studentID}
          setData={setData}
        />
      );
    case 2:
      return (
        <EducationDetails
          handleBack={handleBack}
          handleNext={handleNext}
          completedSteps={completedSteps}
          totalSteps={totalSteps}
          activeStep={activeStep}
          steps={steps}
          handleSkip={handleSkip}
          data={data}
          studentID={studentID}
          setData={setData}
        />
      );
    case 3:
      return (
        <TestScores
          handleBack={handleBack}
          handleNext={handleNext}
          completedSteps={completedSteps}
          totalSteps={totalSteps}
          activeStep={activeStep}
          steps={steps}
          data={data}
          studentID={studentID}
          setData={setData}
        />
      );
    case 4:
      return (
        <CoApplicant
          handleBack={handleBack}
          handleNext={handleNext}
          completedSteps={completedSteps}
          totalSteps={totalSteps}
          activeStep={activeStep}
          steps={steps}
          data={data}
          studentID={studentID}
          setData={setData}
        />
      );
    case 5:
      return (
        <CoApplicant2
          handleBack={handleBack}
          handleNext={handleNext}
          completedSteps={completedSteps}
          totalSteps={totalSteps}
          activeStep={activeStep}
          steps={steps}
          data={data}
          studentID={studentID}
          setData={setData}
        />
      );
    case 6:
      return (
        <UploadDocuments
          handleBack={handleBack}
          handleNext={handleNext}
          completedSteps={completedSteps}
          totalSteps={totalSteps}
          activeStep={activeStep}
          steps={steps}
          data={data}
          studentID={studentID}
          setData={setData}
        />
      );
    default:
      return "Unknown step";
  }
}

function HorizontalNonLinearAlternativeLabelStepper(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [stepName, setStepName] = React.useState("Basic Details");

  const [data, setData] = React.useState("");

  const [completed, setCompleted] = React.useState(new Set());
  const [skipped, setSkipped] = React.useState(new Set());
  const steps = getSteps();
  const getCourseDetails = async () => {
    if (props.todos) {
      setData(props.todos);
    } else {
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        try {
          let response = await Axios({
            method: "get",
            url: `${baseUrl}/student/getApplication/${_id}`,

            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          let data = response.data.data.data;
          setData(data);
        } catch (error) {
          if (error.response)
            console.log(error.response.data.message, "error ");
        }
      }
    }
  };
  useEffect(() => {
    getCourseDetails();
  }, [props.data]);

  const totalSteps = () => {
    return getSteps().length;
  };

  const isStepOptional = (step) => {
    return step === 1;
  };

  const handleSkip = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 2);
  };

  const skippedSteps = () => {
    return skipped.size;
  };

  const completedSteps = () => {
    return completed.size;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps() - skippedSteps();
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed
          // find the first step that has been completed
          steps.findIndex((step, i) => !completed.has(i))
        : activeStep + 1;
    console.log(newActiveStep, "newActiveStep");
    setActiveStep(newActiveStep);

    let steps = getSteps();
    setStepName(steps[activeStep + 1]);
  };

  const handleBack = () => {
    let steps = getSteps();
    setStepName(steps[activeStep - 1]);
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleComplete = () => {
    const newCompleted = new Set(completed);
    newCompleted.add(activeStep);
    setCompleted(newCompleted);

    /**
     * Sigh... it would be much nicer to replace the following if conditional with
     * `if (!this.allStepsComplete())` however state is not set when we do this,
     * thus we have to resort to not being very DRY.
     */
    if (completed.size !== totalSteps() - skippedSteps()) {
      handleNext();
    }
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted(new Set());
    setSkipped(new Set());
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  function isStepComplete(step) {
    return completed.has(step);
  }

  return (
    <div className={classes.root}>
      <div className="steps">
        <Stepper alternativeLabel nonLinear activeStep={activeStep}>
          {steps.map((label, index) => {
            const stepProps = {};
            const buttonProps = {};
            //   if (isStepOptional(index)) {
            //     buttonProps.optional = (
            //       <Typography variant="caption">Optional</Typography>
            //     );
            //   }
            if (isStepSkipped(index)) {
              stepProps.completed = false;
            }
            return (
              <Step key={label} {...stepProps}>
                {loggedInUser && loggedInUser.role == "admin" ? (
                  <StepButton
                    onClick={handleStep(index)}
                    completed={isStepComplete(index)}
                    {...buttonProps}
                  >
                    {label}
                  </StepButton>
                ) : (
                  <StepButton
                    // onClick={handleStep(index)}
                    completed={isStepComplete(index)}
                    {...buttonProps}
                  >
                    {label}
                  </StepButton>
                )}
              </Step>
            );
          })}
        </Stepper>
      </div>
      <div className="tabletSteps">
        <p className="mt-1 mb-1 text-xl">{activeStep + 1}</p>
        <p className="text-2xl font-semibold">{stepName}</p>
      </div>
      <div>
        <div>
          <Typography className="mt-4">
            {getStepContent(
              activeStep,
              handleBack,
              handleNext,
              completedSteps,
              totalSteps,
              activeStep,
              steps,
              handleSkip,
              data,
              props.studentID,
              setData
            )}
          </Typography>
        </div>
      </div>
    </div>
  );
}
export default HorizontalNonLinearAlternativeLabelStepper;
