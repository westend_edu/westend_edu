import React, { useEffect } from "react";
import AutoComplete from "../../../../../../../components/AutoComplete/Index.js";
import RadioButton from "../../../../../../../components/RadioButton/Index.js";
import Axios from "axios";
import Notifier from "../../../../../../../utils/Notifier";
import { useFormik } from "formik";
import baseUrl from "../../../../../../../utils/baseUrl";
import Button from "../../../../../../../components/Button/index";
import TextInput from "../../../../../../../components/TextInput";

import * as Yup from "yup";

import {
  countries,
  Bachelors,
  decision,
  educations,
  departments,
  StatusList,
} from "../../../../../../../utils/Globals";
import { Store } from "../../../../../../../Context";
import { getUser, getToken } from "../../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function CourseDetail(props) {
  const [loading, setloading] = React.useState(false);
  const [checked, setChecked] = React.useState("");

  const handleBachelorChange = (event) => {
    setChecked(event.target.value);
  };
  const {
    handleBack,
    handleNext,
    completedSteps,
    totalSteps,
    activeStep,
    steps,
  } = props;

  const getCourseDetails = async () => {
    if (props.data.data) {
      let data = props.data.data;
      Object.keys(data).map((dat, i) => {
        formik.setFieldValue(dat, data[dat]);
      });
      setChecked(data.apply);
    } else {
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        try {
          let response = await Axios({
            method: "get",
            url: `${baseUrl}/student/getApplication/${_id}`,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          let data = response.data.data.data;
          Object.keys(data).map((dat, i) => {
            if (data[dat]) formik.setFieldValue(dat, data[dat]);
          });
          setChecked(data.apply);
        } catch (error) {
          if (error.response)
            console.log(error.response.data.message, "error ");
        }
      }
    }
  };
  useEffect(() => {
    getCourseDetails();
  }, [props.data, activeStep]);
  const formik = useFormik({
    initialValues: {
      country: "",
      university: "",
      education: "",
      department: "",
      loanAmount: "",
      status: "",
      livingExpense: "",
      collateral: "No",
      collateralNumber: "0",
    },
    validationSchema: Course_YUP,
    onSubmit: async (values) => {
      setloading(true);
      values.apply = checked;
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        if (values.collateral === "No") {
          values.collateralNumber = "0";
        }
        try {
          let response = await Axios({
            method: "post",
            url: `${baseUrl}/student/createCourseDetail/${_id}`,
            data: values,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });

          props.setData(response.data.data);

          handleNext();

          setloading(false);

          // router.push("/profile");
        } catch (err) {
          setloading(false);
          if (err.response) {
            Notifier(err.response.data.message, "error");
          }
        }
      }
    },
  });
  const handleChange = (name, value) => {
    formik.setFieldValue(name, value);
  };

  const handleNumeric = (e) => {
    const { name, value } = e.target;
    formik.setFieldValue(name, value.replace(/\D/g, ""));
  };

  return (
    <div>
      <form className="w-full  ">
        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full  md:w-1/2 px-3 mb-3">
            <label className=" block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
              Applying For :
            </label>
            <RadioButton
              name="bachelors"
              onChange={handleBachelorChange}
              value={checked}
              options={Bachelors}
            />
          </div>
        </div>
        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full  md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Countries
            </label>
            <AutoComplete
              formik={formik}
              data={countries}
              name="country"
              label="Choose a country"
              country={true}
              onChange={handleChange}
              value={formik.values.country}
            />
            <div className="text-danger pt-1">
              {formik.touched.country && formik.errors.country ? (
                <div>{formik.errors.country}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full mb-3 md:w-1/2 px-3">
            <TextInput
              type="text"
              placeholder="University"
              label="University"
              name="university"
              onChange={formik.handleChange}
              value={formik.values.university}
            />

            <div className="text-danger pt-1">
              {formik.touched.university && formik.errors.university ? (
                <div>{formik.errors.university}</div>
              ) : null}
            </div>
          </div>
        </div>

        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full  md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Degree
            </label>
            <AutoComplete
              formik={formik}
              data={departments}
              label="Choose a degree"
              name="department"
              onChange={handleChange}
              value={formik.values.department}
            />
            <div className="text-danger pt-1">
              {formik.touched.department && formik.errors.department ? (
                <div>{formik.errors.department}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full mb-3 md:w-1/2 px-3">
            <label
              className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Area of Study
            </label>
            <AutoComplete
              formik={formik}
              data={educations}
              label="Choose area of study"
              name="education"
              onChange={handleChange}
              value={formik.values.education}
            />
            <div className="text-danger pt-1">
              {formik.touched.education && formik.errors.education ? (
                <div>{formik.errors.education}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full  md:w-1/2 px-3 mb-3">
            <TextInput
              type="text"
              placeholder="Loan Amount (INR)"
              label="Loan Amount (INR)"
              name="loanAmount"
              onChange={handleNumeric}
              value={formik.values.loanAmount}
            />
            <div className="text-danger pt-1">
              {formik.touched.loanAmount && formik.errors.loanAmount ? (
                <div>{formik.errors.loanAmount}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Status
            </label>
            <AutoComplete
              formik={formik}
              data={StatusList}
              label="Choose a status"
              name="status"
              onChange={handleChange}
              value={formik.values.status}
            />
            <div className="text-danger pt-1">
              {formik.touched.status && formik.errors.status ? (
                <div>{formik.errors.status}</div>
              ) : null}
            </div>
          </div>
        </div>
        {/* <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className=" block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Do you have a property for collateral?
            </label>
            <RadioButton
              name="collateral"
              onChange={formik.handleChange}
              value={formik.values.collateral}
              options={decision}
            />
          </div>
          {formik.values.collateral == "Yes" ? (
            <div className="w-full md:w-1/2 px-3 mb-3">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                Approx property valuation
              </label>
              <input
                className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
                type="number"
                min="0"
                max="4"
                placeholder="Approx property valuation"
                name="collateralNumber"
                onChange={formik.handleChange}
                value={formik.values.collateralNumber}
              />
            </div>
          ) : (
            ""
          )}
        </div> */}
        <div className="row">
          <div className="col-lg-12">
            <div className="d-flex text-right">
              <Button
                onClick={handleBack}
                className="no-icon default-btn mr-3 "
                label="Back"
              />
              {activeStep !== steps.length && (
                <Button
                  onClick={formik.handleSubmit}
                  className="no-icon ml-3 default-btn  "
                  label={loading ? "SUBMITTING" : "Next"}
                />
              )}
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

const Course_YUP = Yup.object({
  country: Yup.string().required("Required"),
  university: Yup.string().required("Required"),
  education: Yup.string().required("Required"),
  department: Yup.string().required("Required"),
  loanAmount: Yup.number().required("Required"),
  status: Yup.string().required("Required"),
});
