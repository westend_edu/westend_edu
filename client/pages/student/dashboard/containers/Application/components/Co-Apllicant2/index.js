import React, { useEffect } from "react";
import AutoComplete from "../../../../../../../components/AutoComplete/Index.js";
import Axios from "axios";
import Notifier from "../../../../../../../utils/Notifier";
import baseUrl from "../../../../../../../utils/baseUrl";
import Button from "../../../../../../../components/Button/index";

import { Store } from "../../../../../../../Context";

import { RelationWithCoApplicant } from "../../../../../../../utils/Globals";

import { useFormik } from "formik";
import * as Yup from "yup";
import cookie from "js-cookie";
import { getUser, getToken } from "../../../../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function Index(props) {
  const {
    handleBack,
    handleNext,
    completedSteps,
    totalSteps,
    activeStep,
    steps,
  } = props;

  const getApplication = async () => {
    // if (props.data && props.data.applicantDetail) {
    //   let data = props.data.applicantDetail;
    //   console.log(data, "props.data2");

    //   Object.keys(data).map((dat, i) => {
    //     if (dat == "id" || data == "appId") {
    //       delete data[dat];
    //     }
    //     formik.setFieldValue(dat, data[dat]);
    //   });
    // } else {
    if (loggedInUser) {
      let _id = loggedInUser.id;
      if (props.studentID) {
        _id = props.studentID;
      }

      try {
        let response = await Axios({
          method: "get",
          url: `${baseUrl}/student/getApplication/${_id}`,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        let data = response.data.data.applicantDetail;
        if (data) {
          let fill = data.find((dt) => dt.applicant == "2");
          if (fill) {
            Object.keys(fill).map((dat, i) => {
              if (dat == "id" || dat == "appId") {
                delete fill[dat];
              }
              formik.setFieldValue(dat, fill[dat]);
            });
          }
        }
      } catch (error) {
        if (error.response) console.log(error.response.data.message, "error ");
      }
    }
    //}
  };

  useEffect(() => {
    getApplication();
  }, [props.data, loading, activeStep]);

  const [loading, setloading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      name: "",
      relation: "",
      telephone: "",
      email: "",
      monthlyIncome: "",
      panNumber: "",
      adharNumber: "",
      applicant: "2",
    },
    validationSchema: Detail_YUP,
    onSubmit: async (values) => {
      setloading(true);
      if (loggedInUser) {
        let _id = loggedInUser.id;
        if (props.studentID) {
          _id = props.studentID;
        }
        try {
          let response = await Axios({
            method: "post",
            url: `${baseUrl}/student/createApplicantDetail/${_id}`,
            data: values,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          props.setData(response.data.data);
          setloading(false);
          handleNext();

          // router.push("/profile");
        } catch (err) {
          setloading(false);
          if (err.response) {
            Notifier(err.response.data.message, "error");
          }
        }
      }
    },
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    formik.setFieldValue(name, value.replace(/\D/g, ""));
  };
  const onHandleChange = (name, value) => {
    formik.setFieldValue(name, value);
  };
  return (
    <div>
      <div className="col-lg-12 text-center pb-2">
        <small className="text-danger">
          Your co-applicant can be your Father, Mother, Sister, Brother or any
          other Guardian.{" "}
        </small>
      </div>
      <form className="w-full">
        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Co-Applicant 2's Name
            </label>
            <input
              className="appearance-none block w-full  border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Co-Applicant's Name"
              name="name"
              onChange={formik.handleChange}
              value={formik.values.name}
            />
            <div className="text-danger pt-1">
              {formik.touched.name && formik.errors.name ? (
                <div>{formik.errors.name}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Relation with Co-Applicant 1
            </label>
            <AutoComplete
              data={RelationWithCoApplicant}
              name="relation"
              label="Relation with Co-Applicant"
              onChange={onHandleChange}
              value={formik.values.relation}
            />
            <div className="text-danger pt-1">
              {formik.touched.relation && formik.errors.relation ? (
                <div>{formik.errors.relation}</div>
              ) : null}
            </div>
          </div>

          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Co-Applicant 2's Phone Number
            </label>
            <input
              className="appearance-none block w-full  border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Co-Applicant's Phone Number"
              name="telephone"
              onChange={handleChange}
              value={formik.values.telephone}
            />
            <div className="text-danger pt-1">
              {formik.touched.telephone && formik.errors.telephone ? (
                <div>{formik.errors.telephone}</div>
              ) : null}
            </div>
          </div>
        </div>

        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-xs font-bold mb-2"
              htmlFor="grid-last-name"
            >
              Co-Applicant 2's Email
            </label>
            <input
              className="appearance-none block w-full  border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
              id="grid-last-name"
              type="text"
              placeholder="Co-Applicant's Email"
              name="email"
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            <div className="text-danger pt-1">
              {formik.touched.email && formik.errors.email ? (
                <div>{formik.errors.email}</div>
              ) : null}
            </div>
          </div>

          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Co-Applicant 2's Monthly Income (INR)
            </label>
            <input
              className="appearance-none block w-full  border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Monthly Income (INR)"
              name="monthlyIncome"
              onChange={handleChange}
              value={formik.values.monthlyIncome}
            />
            <div className="text-danger pt-1">
              {formik.touched.monthlyIncome && formik.errors.monthlyIncome ? (
                <div>{formik.errors.monthlyIncome}</div>
              ) : null}
            </div>
          </div>
        </div>

        <div className="2xl:flex 2xl:flex-row xl:flex xl:flex-row lg:flex lg:flex-row md:flex md:flex-row no-wrap sm:flex sm:flex-col -mx-3 mb-0">
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Co-Applicant 2's Pan Number
            </label>
            <input
              className="appearance-none block w-full  border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Pan Number"
              name="panNumber"
              onChange={formik.handleChange}
              value={formik.values.panNumber}
            />
            <div className="text-danger pt-1">
              {formik.touched.panNumber && formik.errors.panNumber ? (
                <div>{formik.errors.panNumber}</div>
              ) : null}
            </div>
          </div>
          <div className="w-full md:w-1/2 px-3 mb-3">
            <label
              className="inputLabel block uppercase tracking-wide text-xs font-bold mb-2"
              htmlFor="grid-first-name"
            >
              Co-Applicant 2's Aadhar Number
            </label>
            <input
              className="appearance-none block w-full  border border-red-500 rounded py-3 px-4 mb-0 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="Aadhar Number"
              name="adharNumber"
              onChange={handleChange}
              value={formik.values.adharNumber}
            />
            <div className="text-danger pt-1">
              {formik.touched.adharNumber && formik.errors.adharNumber ? (
                <div>{formik.errors.adharNumber}</div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="xl-flex lg:flex sm:flex md:flex xl:flex-row lg:flex-row md:flex-row sm:flex-col lg:justify-start md:justify-start sm:justify-start">
              <div className="my-2">
                <Button
                  onClick={handleBack}
                  className="no-icon default-btn lg:mr-3 md:mr-3 sm:mr-0  "
                  label="Back"
                />
              </div>
              <div className="my-2">
                {activeStep !== steps.length && (
                  <Button
                    onClick={formik.handleSubmit}
                    className="no-icon lg:mr-3 md:mr-3 sm:mr-0 lg:my-0   default-btn  "
                    label={loading ? "SUBMITTING" : "Next"}
                  />
                )}
              </div>
              <div className="my-2">
                <Button
                  onClick={() => props.handleNext()}
                  className="no-icon lg:mr-3 md:mr-3 sm:mr-0  default-btn  "
                  label={"Skip"}
                />
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
const Detail_YUP = Yup.object({
  relation: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  name: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),

  telephone: Yup.number().required("Required"),
  email: Yup.string().email("Invalid email address").required("Required"),

  monthlyIncome: Yup.number().required("Required"),
  panNumber: Yup.string().required("Required"),
  adharNumber: Yup.number().required("Required"),
});
