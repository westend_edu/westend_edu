import React from "react";
import Axios from "axios";

import Sidebar from "./components/sidebar/index.js";
import Application from "./containers/Application/index.js";
import ActiveLoans from "./containers/ActiveLoans/index.js";
import Tools from "./containers/Tools/index.js";
import Faq from "./containers/Faq/index.js";
import EditProfile from "../../edit-profile";
import ChangePassword from "../../edit-password";
import { parseCookies } from "nookies";
import * as cookie from "cookie";

import baseUrl from "../../../utils/baseUrl";
const Index = (props) => {
  const [activeTab, setActiveTab] = React.useState(1);
  const [activeIndex, setactiveIndex] = React.useState(1);

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
    setactiveIndex(tab);
  };
  function Page() {
    if (activeTab == 1) {
      return <Application todos={props.todos} />;
    } else if (activeTab == 2) {
      return <ActiveLoans setActiveTab={setActiveTab} />;
    } else if (activeTab == 3) {
      return <Tools />;
    } else if (activeTab == 4) {
      return <Faq />;
    } else if (activeTab == 5) {
      return <EditProfile />;
    } else if (activeTab == 6) {
      return <ChangePassword />;
    }
  }
  return (
    <React.Fragment>
      <div className="ptb-100">
        <div className="container">
          <div className="row">
            <Sidebar toggle={toggle} isActive={activeIndex} />

            <div className="col-lg-9">
              <div className="td-text-area">{Page()}</div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Index;
