import Step1 from "@/components/VisaProcessSteps/Step1"
import Step2 from "@/components/VisaProcessSteps/Step2"
import Step3 from "@/components/VisaProcessSteps/Step3"
import Step4 from "@/components/VisaProcessSteps/Step4"
import React, { useState } from "react"
import { StateMachineProvider, createStore } from "little-state-machine"
import Step6 from "@/components/VisaProcessSteps/Step6"
import Step5 from "@/components/VisaProcessSteps/Step5"
import Step7 from "@/components/VisaProcessSteps/Step7"
import Step8 from "@/components/VisaProcessSteps/Step8"

createStore({})

const VisaProcess = () => {
	const [step, setStep] = useState(1)
	const nextFormStep = () => setStep((currentStep) => currentStep + 1)
	const prevFormStep = () => setStep((currentStep) => currentStep - 1)
	return (
		<StateMachineProvider>
			<div className="w-full flex flex-col items-center justify-between">
				{step === 1 && (
					<Step1
						step={step}
						nextFormStep={nextFormStep}
						prevFormStep={prevFormStep}
					/>
				)}
				{step === 2 && (
					<Step2
						step={step}
						nextFormStep={nextFormStep}
						prevFormStep={prevFormStep}
					/>
				)}
				{step === 3 && (
					<Step3
						step={step}
						nextFormStep={nextFormStep}
						prevFormStep={prevFormStep}
					/>
				)}
				{step === 4 && (
					<Step4
						step={step}
						nextFormStep={nextFormStep}
						prevFormStep={prevFormStep}
					/>
				)}

				{step === 5 && (
					<Step5
						step={step}
						nextFormStep={nextFormStep}
						prevFormStep={prevFormStep}
					/>
				)}
				{step === 6 && (
					<Step6
						step={step}
						nextFormStep={nextFormStep}
						prevFormStep={prevFormStep}
					/>
				)}
				{step === 7 && (
					<Step7
						step={step}
						nextFormStep={nextFormStep}
						prevFormStep={prevFormStep}
					/>
				)}
				{step === 8 && <Step8 />}

				<img src="/images/vector-graphic2.svg" className="w-full" />
			</div>
		</StateMachineProvider>
	)
}

export default VisaProcess
