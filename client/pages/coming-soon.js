import React from 'react';
import Link from 'next/link';

const ComingSoon = () => {
   
    return (
        <div className="coming-soon-area">
            <div className="d-table">
                <div className="d-table-cell">
                    <div className="coming-soon-content">
                        <h2>We Are Launching Soon</h2>

                     
                        <form className="newsletter-form" data-toggle="validator">
                            <div className="form-group">
                                <input type="email" className="input-newsletter" placeholder="Enter your email" name="EMAIL" required />
                                <span className="label-title">
                                    <i className='bx bx-envelope'></i>
                                </span>
                            </div>

                            <button type="submit" className="default-btn">
                                <i className="flaticon-user"></i> Subscribe <span></span>
                            </button>
 
                            <p>If you would like to be notified when the app is live, Please subscribe to our mailing list.</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ComingSoon;