import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../../utils/baseUrl";
import { useRouter } from "next/router";
import Axios from "axios";
import Notifier from "../../utils/Notifier";
import ErrorPage from "../404";
import cookie from "js-cookie";
import PhoneInput from "../../components/CustomPhoneInput.js";

export default function Index(props) {
  const router = useRouter();

  let cooky = cookie.get("user");
  let loggedInUser = cooky && JSON.parse(cooky);
  const [isloading, setisloading] = React.useState(false);
  const [partner_name, setPartnerName] = React.useState("");
  const [Image, setImage] = React.useState("");
  const [error, setError] = React.useState(false);

  React.useEffect(() => {
    getPartnerDetails();
  }, []);
  const getPartnerDetails = async () => {
    let _id = router.query.link;
    try {
      let response = await Axios({
        method: "get",
        url: `${baseUrl}/partner/?token=${_id}`,
      });
      const { image, company_name } = response.data.data;
      setPartnerName(company_name);
      setImage(image);
    } catch (err) {
      if (err.response) {
        if (err.response.data.message == "Token invalid!") {
          setError(true);
        }
        Notifier(err.response.data.message, "error");
      }
    }
  };
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
    },
    validationSchema: LOGIN_YUP,
    onSubmit: async (values) => {
      setisloading(true);
      let _id = router.query.link;

      values.verifyPartnerToken = _id;
      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/partner/createPartnerLead`,
          data: values,
        });
        Notifier(response.data.message, "success");
        setisloading(false);
        formik.handleReset();
      } catch (err) {
        Notifier(err.response.data.message, "error");
        setisloading(false);
      }
    },
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "phone") {
      formik.setFieldValue(name, value.replace(/\D/g, ""));
    } else {
      formik.setFieldValue(name, value);
    }
  };
  if (error == true) {
    return <ErrorPage />;
  }
  const onNumberChange = (value) => {
    formik.setFieldValue("phone", value);
  };
  return (
    <React.Fragment>
      <div className="linkPage profile-authentication-area pb-70">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 col-md-12">
              <div className="col-lg-12 text-left pb-2">
                <div className=" text-center pt-2 pb-4">
                  <h2 className="text-uppercase text-bold">
                    <b> WESTEND | {partner_name} </b>
                  </h2>
                </div>
                <div className="d-flex justify-content-center">
                  {loggedInUser && (
                    <img src={Image ? Image : "/images/user9.png"} />
                  )}
                </div>

                <div className="mt-2">
                  <h5>Create New Lead</h5>
                </div>
              </div>

              <form className="w-full ">
                <div className="flex no-wrap -mx-3 mb-0">
                  <div className="w-full pl-3 mb-6 md:mb-0">
                    <input
                      className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-2 mt-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                      type="text"
                      placeholder="First Name"
                      name="firstName"
                      onChange={handleChange}
                      value={formik.values.firstName}
                    />
                    <div className="text-danger pt-1">
                      {formik.touched.firstName && formik.errors.firstName ? (
                        <div>{formik.errors.firstName}</div>
                      ) : null}
                    </div>
                  </div>
                  <div className="w-full px-3">
                    <input
                      className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-2 mt-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                      type="text"
                      placeholder="Last Name"
                      name="lastName"
                      onChange={handleChange}
                      value={formik.values.lastName}
                    />
                    <div className="text-danger pt-1">
                      {formik.touched.lastName && formik.errors.lastName ? (
                        <div>{formik.errors.lastName}</div>
                      ) : null}
                    </div>
                  </div>
                </div>
                <div className="flex no-wrap -mx-3 mb-0">
                  <div className="w-full px-3 mb-6 md:mb-0">
                    <PhoneInput
                      value={formik.values.phone}
                      onChange={onNumberChange}
                    />
                    <div className="text-danger pt-1">
                      {formik.touched.phone && formik.errors.phone ? (
                        <div>{formik.errors.phone}</div>
                      ) : null}
                    </div>
                  </div>
                </div>

                <div className="flex no-wrap -mx-3 mb-0">
                  <div className="w-full px-3">
                    <input
                      className="appearance-none block w-full  text-gray-700 border border-gray-200 rounded py-3 px-4 mb-2 mt-1 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                      id="grid-last-name"
                      type="text"
                      placeholder="Email"
                      name="email"
                      onChange={formik.handleChange}
                      value={formik.values.email}
                    />
                    <div className="text-danger pt-1">
                      {formik.touched.email && formik.errors.email ? (
                        <div>{formik.errors.email}</div>
                      ) : null}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-12">
                    <div className="d-flex pt-2">
                      <button
                        onClick={formik.handleSubmit}
                        className="no-icon default-btn"
                      >
                        {isloading ? "SUBMITTING" : "Submit"}
                        <span></span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
const LOGIN_YUP = Yup.object({
  firstName: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  lastName: Yup.string()
    .max(100, "Must be less than 100 characters")
    .required("Required"),
  phone: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
  email: Yup.string().email("Invalid email address").required("Required"),
});
