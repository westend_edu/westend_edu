import React, { useEffect } from "react";
import {
  faPlus,
  faSort,
  faDownload,
  faCalendarAlt,
} from "@fortawesome/free-solid-svg-icons";
import FontAwesomeIcon from "../../../../components/FontAwesomeIcon/index.js";
import List from "../../../../components/List/index.js";
import DatePicker from "../../../../components/DatePicker/index.js";
import DateRangePicker from "../../../../components/DateRangePicker/index.js";

import Popover from "@material-ui/core/Popover";
import jsPDF from "jspdf";
import "jspdf-autotable";
import moment from "moment";
import Notifier from "../../../../utils/Notifier";
import { Store } from "../../../../Context";
import baseUrl from "../../../../utils/baseUrl";
import Axios from "axios";
import { getUser, getToken } from "../../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
export default function filter(props) {
  const [pickerValue, setpickerValue] = React.useState([null, null]);

  const { setData, createToggle } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [searchInput, setsearchInput] = React.useState("");
  const [date, setDate] = React.useState("");
  let checkedArr = ["student_name", "phone", "email"];

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const [checked, setChecked] = React.useState();
  useEffect(() => {
    let checkedArray = ["student_name", "phone", "email"];
    setChecked(checkedArray);
  }, []);
  useEffect(() => {
    if (date == "" && searchInput == "") {
      getUsers();
    }
  }, [searchInput, date]);
  const getUsers = async () => {
    try {
      let res;
      res = await Axios.get(`${baseUrl}/partner/${loggedInUser.id}`);

      props.setData(res.data.data);
    } catch (error) {
      console.log(error, "Error-->");
    }
  };

  const handleChange = async (e) => {
    let value = e.target.value;

    setsearchInput(value);
    let _id = loggedInUser.id;
    if (props.partnerID) {
      _id = props.partnerID;
    }
    let url = `${baseUrl}/partner/search/filter?id=${_id}&q=${value}&date=${date}&email=${checked.includes(
      "email"
    )}&name=${checked.includes("student_name")}&phone=${checked.includes(
      "phone"
    )}`;

    try {
      let response = await Axios({
        method: "get",
        url: url,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      props.setData(response.data.data);

      // router.reload();
    } catch (err) {
      if (err.response) {
        Notifier(err.response.data.message, "error");
      }
    }
  };

  const handleDateChange = async (dates, datesStrings, info) => {
    let _id = loggedInUser.id;
    if (props.partnerID) {
      _id = props.partnerID;
    }
    if (datesStrings[0] == "" || datesStrings[1] == "") {
      setDate("");
    } else {
      let value = datesStrings[0] + " - " + datesStrings[1];
      // 10/20/2021 - 11/22/2021
      setDate(value);
      let url;
      if (searchInput !== "") {
        url = `${baseUrl}/partner/search/searchByDate/?id=${_id}&date=${value}`;
      } else {
        url = `${baseUrl}/partner/search/filter?id=${_id}&q=${searchInput}&date=${value}&email=${checked.includes(
          "email"
        )}&name=${checked.includes("student_name")}&phone=${checked.includes(
          "phone"
        )}`;
      }
      try {
        let response = await Axios({
          method: "get",
          url: url,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        props.setData(response.data.data);

        // router.reload();
      } catch (err) {
        if (err.response) {
          Notifier(err.response.data.message, "error");
        }
      }
    }
  };

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };

  // expport data
  const generatePDF = () => {
    var today = moment(new Date()).format("DD/MM/YYYY");
    var newdate = "Date: " + today;
    const doc = new jsPDF();
    const tableColumn = ["Student_Name", "Phone", "Date/Time", "Email"];
    const tableRows = [];
    // console.log('tickets', tickets);
    props.data.map((ticket) => {
      const ticketData = [
        ticket.name,
        ticket.phone,
        moment(ticket.createdAt).format("DD/MM/YYYY HH:mm"),
        ticket.email,
        //   format(new Date(ticket.updated_at), 'yyyy-MM-dd'),
      ];
      tableRows.push(ticketData);
    });
    // doc.text(145, 8, newdat);
    doc.setFont("", "bold");
    doc.text("Student Data", 105, 10, null, null, "center");
    doc.text(newdate, 160, 25);
    // doc.text('Admin’s client :  ', 14, 35);
    doc.setFont("", "bold");
    // doc.text(Partner_Name, 75, 30, null, null, 'right');
    doc.autoTable(tableColumn, tableRows, {
      startY: 54,
      theme: "grid",
      styles: {
        halign: "center",
      },
    });
    doc.setFontSize(10);
    // doc.text('Total Re-Invoicing : ', 185, 100, null, null, 'right');
    // doc.text('Total Amount : ', 185, 105, null, null, 'right');
    doc.setTextColor(255, 0, 0);
    // doc.text(totalReinvoices, 195, 100, null, null, 'right');
    // doc.text(totalPrice, 195, 105, null, null, 'right');
    const date = Date().split(" ");
    const dateStr = date[0] + "_" + date[1] + "_" + date[2];
    // doc.text('Hello downloaded the  tickets within the last one month.', 14, 30);
    doc.save(`${dateStr}.pdf`);
  };

  function ExportToExcel(type = "xlsx", id) {
    var data = document.getElementById(id);
    let name = Math.floor(Math.random() * 1000000000);
    var file = XLSX.utils.table_to_book(data, { sheet: "sheet1" });

    XLSX.write(file, { bookType: type, bookSST: true, type: "base64" });

    XLSX.writeFile(file, `${name}.` + type);
  }
  // ********
  return (
    <div className="d-flex mb-2 justify-content-between">
      <div>
        <button
          // disabled={activeStep === 0}
          onClick={createToggle}
          className="default-btn3"
        >
          <FontAwesomeIcon icon={faPlus} className="mr-1" />
          Create<span></span>
        </button>
      </div>
      <div className=" d-flex">
        <div className="d-flex searchInput">
          <input
            className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
            type="text"
            placeholder="Search"
            name="searchInput"
            value={searchInput}
            onChange={handleChange}
          />
          <div>
            <FontAwesomeIcon
              color="#d33"
              aria-describedby={id}
              icon={faSort}
              onClick={handleClick}
            />
          </div>
        </div>
        <div className="ml-2">
          {/* <DatePicker
            label="Date"
            name="date"
            value={date}
            onChange={handleDateChange}
          /> */}
          <DateRangePicker onChange={handleDateChange} />
        </div>
        <div className="ml-2 mt-3 faAngleDoubleDown">
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
          >
            <List onClick={handleToggle} data={checkedArr} checked={checked} />
          </Popover>

          <FontAwesomeIcon
            color="#152f68"
            aria-describedby={id}
            icon={faDownload}
            onClick={() => ExportToExcel("xlsx", props.tableID)}
            className="ml-1"
          />
        </div>
      </div>
    </div>
  );
}
