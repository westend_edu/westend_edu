import { Store } from "../../../Context";
import React, { useState, useEffect } from "react";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import imageUrl from "../../../utils/imageUrl";
import baseUrl from "../../../utils/baseUrl";
import Notifier from "../../../utils/Notifier";
import Axios from "axios";
import cookie from "js-cookie";
import { parseCookies } from "nookies";
import { getUser, getToken } from "../../../utils/auth";
const token = getToken();
const loggedInUser = getUser();
const EditProfile = () => {
  const [photoUri, setPhotoUri] = useState("");
  const [loading, setloading] = React.useState(false);

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    company_name: "",
    website: "",
    reason: "",
    email: "",
    phone: "",
    image: "",
  });
  useEffect(() => {
    if (loggedInUser) {
      let {
        firstName,
        lastName,
        email,
        phone,
        image,
        company_name,
        website,
        reason,
      } = loggedInUser;
      setFormData({
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone,
        image: image,
        company_name: company_name,
        website: website,
        reason: reason,
      });
    }
  }, [loggedInUser]);
  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "image") {
      let uri = URL.createObjectURL(e.target.files[0]);
      let file = e.target.files[0];
      setPhotoUri(uri);
      setFormData({ ...formData, [name]: file });
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    setloading(true);
    let {
      firstName,
      lastName,
      email,
      phone,
      image,
      company_name,
      website,
      reason,
    } = formData;

    const form_data = new FormData();
    form_data.append("firstName", firstName);
    form_data.append("lastName", lastName);

    form_data.append("email", email);
    form_data.append("phone", phone);
    form_data.append("image", image);
    form_data.append("company_name", company_name);
    form_data.append("website", website);
    form_data.append("reason", reason);

    try {
      let response = await Axios({
        method: "put",
        url: `${baseUrl}/auth/update/${loggedInUser.id}`,
        data: form_data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setloading(false);
      Notifier(response.data.message, "success");
      cookie.set("user", JSON.stringify(response.data.data));

      // router.push("/profile");
    } catch (err) {
      setloading(false);
      if (err.response) {
        Notifier(err.response.data.message, "error");
      }
    }
  };
  return (
    <React.Fragment>
      <div id="editProfile">
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <div className="border-box">
                <form onSubmit={handleSubmit}>
                  <div className="form-group">
                    <label>First Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="email"
                      name="firstName"
                      onChange={handleChange}
                      value={formData.firstName}
                    />
                  </div>
                  <div className="form-group">
                    <label>Last Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="email"
                      name="lastName"
                      onChange={handleChange}
                      value={formData.lastName}
                    />
                  </div>
                  <div className="form-group">
                    <label>Phone</label>
                    <input
                      type="text"
                      className="form-control"
                      id="email"
                      onChange={handleChange}
                      name="phone"
                      value={formData.phone}
                    />
                  </div>
                  <div className="form-group">
                    <label>Email address</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      onChange={handleChange}
                      name="email"
                      value={formData.email}
                    />
                  </div>
                  <div className="form-group">
                    <label>company Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="company_name"
                      disabled
                      onChange={handleChange}
                      name="company_name"
                      value={formData.company_name}
                    />
                  </div>
                  <div className="form-group">
                    <label>Website</label>
                    <input
                      type="text"
                      className="form-control"
                      id="website"
                      onChange={handleChange}
                      name="website"
                      value={formData.website}
                    />
                  </div>
                  <div className="form-group">
                    <label>Reason</label>
                    <textarea
                      id="noter-text-area"
                      placeholder="Reason"
                      className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                      name="reason"
                      onChange={handleChange}
                      value={formData.reason}
                    />
                  </div>

                  <button type="submit" className="default-btn mt-10">
                    {loading ? "Updating..." : "Update"}
                    <span></span>
                  </button>
                </form>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="user-profile">
                <img src={photoUri ? photoUri : `${formData.image}`} />
                <label className="text-center icon" htmlFor="upload-button">
                  <FontAwesomeIcon icon={faEdit} />
                </label>
                <input
                  type="file"
                  id="upload-button"
                  name="image"
                  accept="image/*"
                  style={{ display: "none" }}
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <Footer /> */}
    </React.Fragment>
  );
};

export default EditProfile;
