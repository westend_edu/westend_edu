import React from "react";
import { getUser, getToken } from "../../../../../utils/auth";
import Button from "../../../../../components/Button/index";
import { faLongArrowAltLeft } from "@fortawesome/free-solid-svg-icons";

const token = getToken();
const loggedInUser = getUser();
export default function index(props) {
  return (
    <div className=" col-lg-3">
      <div className="td-sidebar">
        <ul>
          <li>
            <a
              onClick={() => props.toggle(1)}
              className={props.isActive === 1 ? "active" : "album"}
            >
              Students
            </a>
          </li>
          <li>
            <a
              className={props.isActive === 2 ? "active" : "album"}
              onClick={() => props.toggle(2)}
            >
              Tools
            </a>
          </li>
          <li>
            <a
              className={props.isActive === 3 ? "active" : "album"}
              onClick={() => props.toggle(3)}
            >
              FAQ's
            </a>
          </li>
          {loggedInUser &&
          loggedInUser.role == "admin" &&
          props.partnerTab &&
          props.partnerTab === true ? (
            ""
          ) : (
            <>
              <li>
                <a
                  onClick={() => props.toggle(4)}
                  className={props.isActive === 4 ? "active" : "album"}
                >
                  Edit Profile
                </a>
              </li>
              <li>
                <a
                  onClick={() => props.toggle(5)}
                  className={props.isActive === 5 ? "active" : "album"}
                >
                  Change Password
                </a>
              </li>
            </>
          )}
        </ul>
      </div>
      {loggedInUser && loggedInUser.role == "partner" ? (
        ""
      ) : (
        <div className="d-flex mt-5 justify-content-center">
          <Button
            onClick={props.handleBack}
            className="no-icon default-btn"
            icon={faLongArrowAltLeft}
            label="Back"
          />
        </div>
      )}
    </div>
  );
}
