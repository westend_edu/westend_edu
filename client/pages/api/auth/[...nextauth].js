import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import FacebookProvider from "next-auth/providers/facebook";
import LinkedInProvider from "next-auth/providers/linkedin";
import baseUrl from "../../../utils/baseUrl";
import { useRouter } from "next/router";
import Axios from "axios";
import { handleLogin } from "../../../utils/auth";
import Router from "next/router";
import cookie from "js-cookie";

export default NextAuth({
  // Configure one or more authentication providers
  session: {
    jwt: true,
  },
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      authorizationUrl:
        "https://accounts.google.com/o/oauth2/v2/auth?prompt=consent&access_type=offline&response_type=code",
    }),
    FacebookProvider({
      clientId: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
    }),

    {
      id: "linkedin",
      name: "LinkedIn",
      type: "oauth",
      version: "2.0",
      scope: "r_liteprofile r_emailaddress",
      params: {
        grant_type: "authorization_code",
      },
      accessTokenUrl: "https://www.linkedin.com/oauth/v2/accessToken",
      requestTokenUrl: "https://www.linkedin.com/oauth/v2/authorization",
      authorizationUrl:
        "https://www.linkedin.com/oauth/v2/authorization?response_type=code",
      profileUrl:
        "https://api.linkedin.com/v2/me?projection=(id,localizedFirstName,localizedLastName)",
      async profile(profile, tokens) {
        const res = await fetch(
          "https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))",
          {
            headers: {
              Authorization: `Bearer ${tokens.accessToken}`,
            },
          }
        );
        const data = await res.json();
        const email = data?.elements[0]["handle~"].emailAddress;
        return {
          id: profile.id,
          name: profile.localizedFirstName + " " + profile.localizedLastName,
          email: email,
          image: null,
        };
      },
      clientId: process.env.LINKEDIN_CLIENT_ID,
      clientSecret: process.env.LINKEDIN_CLIENT_SECRET,
    },
  ],
  callbacks: {
    async signIn(values, account, profile) {
      if (
        account.provider === "google" &&
        profile.verified_email === true &&
        profile.email.endsWith("@gmail.com")
      ) {
        let firstName = profile.given_name;
        let lastName = profile.family_name;
        let email = profile.email;
        let formData = {
          firstName: firstName,
          lastName: lastName,
          email: email,
          phone: "",
        };

        try {
          let response = await Axios.post(`${baseUrl}/auth/socialLogin`, {
            email: email,
          });
        } catch (err) {
          if (
            err.response.data.message.trim() ==
            "No user found with that Email!".trim()
          ) {
            try {
              let response = await Axios.post(
                `${baseUrl}/auth/signup`,
                formData
              );
            } catch (error) {
              console.log(error.response.data.message, "error");
            }
          }
        }
        return true;
        // return true;
      } else if (account.provider == "facebook") {
        let newProfile = values.name.split(" ");
        let firstName = newProfile[0];
        let lastName = newProfile[1];
        let email = values.email;
        let formData = {
          firstName: firstName,
          lastName: lastName,
          email: email,
          phone: "",
          image: values.image,
        };

        try {
          let response = await Axios.post(`${baseUrl}/auth/socialLogin`, {
            email: email,
          });
        } catch (err) {
          if (
            err.response.data.message.trim() ==
            "No user found with that Email!".trim()
          ) {
            try {
              let response = await Axios.post(
                `${baseUrl}/auth/signup`,
                formData
              );
            } catch (error) {
              console.log(error.response.data.message, "error");
            }
          }
        }
        return true;
      } else if (account.provider == "linkedin") {
        let firstName = profile.localizedFirstName;
        let lastName = profile.localizedLastName;
        let email = values.email;
        let formData = {
          firstName: firstName,
          lastName: lastName,
          email: email,
          phone: "",
        };

        try {
          let response = await Axios.post(`${baseUrl}/auth/socialLogin`, {
            email: email,
          });
        } catch (err) {
          if (
            err.response.data.message.trim() ==
            "No user found with that Email!".trim()
          ) {
            try {
              let response = await Axios.post(
                `${baseUrl}/auth/signup`,
                formData
              );
            } catch (error) {
              console.log(error.response.data.message, "error");
            }
          }
        }

        return true;
      } else {
        return false;
      }
    },
    async session(session = { user: {} }, user = {}) {
      let newUser;
      let newToken;
      let successMessage;
      let errorMessage = "";
      if (Object.keys(session).length > 0 && session.token === undefined) {
        try {
          let response = await Axios.post(`${baseUrl}/auth/socialLogin`, {
            email: session.user.email,
          });
          if (response.data) {
            const { token, user } = response.data.data;
            newUser = user;
            newToken = token;
          }
          successMessage = response.data.message;
        } catch (err) {
          if (err) {
            errorMessage = err.response.data.message;
          }
          console.log(err.response.data.message, "error");
        }
      }
      return {
        ...session,
        user: newUser,
        token: newToken,
        successMessage: successMessage,
        errorMessage: errorMessage,
      };
    },
    // redirect({ url, baseUrl }) {
    //   return url.startsWith(baseUrl) ? url : baseUrl;
    // },
  },
});
