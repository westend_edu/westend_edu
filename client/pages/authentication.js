import React from "react"
import PageBanner from "../components/Common/PageBanner"
import LoginForm from "./reset-password.js"
import RegisterForm from "../components/Authentication/RegisterForm"

const Authentication = () => {
  return (
    <React.Fragment>
      <div className="profile-authentication-area ptb-100">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 col-md-12">
              <LoginForm />
            </div>

           
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Authentication
