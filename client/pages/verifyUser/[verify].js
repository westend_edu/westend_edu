import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../../utils/baseUrl";
import { useRouter } from "next/router";
import Axios from "axios";
import Notifier from "../../utils/Notifier";
import Preloader from "../../components/_App/Preloader";

import { handleLogin } from "../../utils/auth";

export default function Index(props) {
  const router = useRouter();
  const [isloading, setisloading] = React.useState(true);

  React.useEffect(() => {
    if (router.query && router.query.verify) {
      handleVerify();
    }
  }, []);

  const handleVerify = async () => {
    let _id = router.query.verify;
    try {
      setisloading(true);
      let response = await Axios({
        method: "post",
        url: `${baseUrl}/auth/verifyStudent`,
        data: { passwordToken: _id },
      });
      const { token, user } = response.data.data;

      let path;
      if (user.role === "student") {
        path = "/student/dashboard";
      } else if (user.role === "admin") {
        path = "/admin/dashboard";
      } else {
        path = "/partner/dashboard";
      }

      handleLogin(token, user, path);
      Notifier(response.data.message, "success");

      setTimeout(() => {
        setisloading(false);
      }, 1000);
    } catch (err) {
      setTimeout(() => {
        setisloading(false);
      }, 1000);

      Notifier(err.response.data.message, "error");
    }
  };

  // const [isloading, setisloading] = React.useState(false);
  // const [resetPassword, setresetPassword] = React.useState(true);

  // const toggle = () => setresetPassword(!resetPassword);
  // const formik = useFormik({
  //   initialValues: {
  //     password: "",
  //     confirm_password: "",
  //   },
  //   validationSchema: LOGIN_YUP,
  //   onSubmit: async (values) => {
  //     setisloading(true);
  //     let _id = router.query.verify;
  //     try {
  //       let response = await Axios({
  //         method: "put",
  //         url: `${baseUrl}/auth/set-password`,
  //         data: { password: values.password, passwordToken: _id },
  //       });
  //       const { token, user } = response.data.data;

  //       let path;
  //       if (user.role === "student") {
  //         path = "/student/dashboard";
  //       } else if (user.role === "admin") {
  //         path = "/admin/dashboard";
  //       } else {
  //         path = "/partner/dashboard";
  //       }

  //       handleLogin(token, user, path);
  //       Notifier(response.data.message, "success");
  //       setisloading(false);

  //       formik.handleReset();
  //     } catch (err) {
  //       Notifier(err.response.data.message, "error");
  //       setisloading(false);
  //     }
  //   },
  // });
  if (isloading) {
    return <Preloader />;
  }
  return (
    <React.Fragment>
      <div className="profile-authentication-area ptb-100">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 col-md-12">
              <div className="col-lg-12 text-center pb-2">
                <div className="mb-4">
                  <h4>Your Email is Successfully Verified</h4>
                  {/* <h4>Please set your password</h4> */}
                </div>
              </div>

              {/* <form onSubmit={formik.handleSubmit}>
                <>
                  <div className="form-group first">
                    
                    <input
                      type="password"
                      className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                      placeholder="Password"
                      id="password"
                      name="password"
                      onChange={formik.handleChange}
                      value={formik.values.password}
                    />
                    <div className="text-danger pt-1">
                      {formik.touched.password && formik.errors.password ? (
                        <div className="formikError">
                          {formik.errors.password}
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div className="form-group first">
                    
                    <input
                      type="password"
                      className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                      placeholder="Confirm Password"
                      id="confirm_password"
                      name="confirm_password"
                      onChange={formik.handleChange}
                      value={formik.values.confirm_password}
                    />
                    <div className="text-danger pt-1">
                      {formik.touched.confirm_password &&
                      formik.errors.confirm_password ? (
                        <div className="formikError">
                          {formik.errors.confirm_password}
                        </div>
                      ) : null}
                    </div>
                  </div>
                </>

                <button type="submit" className="default-btn btn-block">
                  {isloading ? "SUBMITTING..." : "SUBMIT"}
                </button>
              </form>
           */}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
const LOGIN_YUP = Yup.object({
  password: Yup.string()
    .min(6, "Must be 6 characters long")
    .required("This field is required"),
  confirm_password: Yup.string()
    .when("password", {
      is: (val) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "Both password need to be the same"
      ),
    })
    .min(6, "Must be 6 characters long")
    .required("Required"),
});
