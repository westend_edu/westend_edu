import React, { useState } from "react";

import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../../utils/baseUrl";
import Axios from "axios";
import Notifier from "../../utils/Notifier";
import { handleLogin } from "../../utils/auth";
import Signup from "./signup";
import { signIn } from "next-auth/client";
import PhoneInput from "../../components/CustomPhoneInput.js";
import Router from "next/router";

export default function Index(props) {
  const [loginState, setLoginState] = useState(true);
  const [loading, setloading] = React.useState(false);
  const [isNumber, setisNumber] = React.useState(false);

  const login_formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: isNumber == true ? NUMBER_YUP : LOGIN_YUP,
    onSubmit: async (values) => {
      setloading(true);
      if (isNumber) {
        values.phone = values.email;
      } else {
        values.phone = "";
      }

      try {
        let response = await Axios({
          method: "post",
          url: `${baseUrl}/auth/otpLogin`,
          data: values,
        });
        Notifier(response.data.message, "success");
        setloading(false);
        props.loginToggle();
        props.verifyotpToggle();
        formik.handleReset();
      } catch (err) {
        Notifier(err.response.data.message, "error");
        setloading(false);
      }
    },
  });
  const handleChange = (e) => {
    const { value, name } = e.target;
    if (value !== "" && isNaN(value) == false) {
      setisNumber(true);
      login_formik.setFieldValue("email", value);
      return true;
    } else {
      setisNumber(false);
    }
    login_formik.setFieldValue(name, value);
  };
  const onNumberChange = (value) => {
    if (value === "") {
      setisNumber(false);
      login_formik.setFieldValue("email", "");
    } else {
      login_formik.setFieldValue("email", value);
    }
  };
  return (
    <div>
      <div className="loginContent">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 order-md-2 imgSection"></div>
            <div className="col-md-12 col-lg-6 col-sm-12 col-xs-12 loginContents">
              <div className="row justify-loginContent-center">
                <div className="col-md-12">
                  <div className="col-lg-12 text-center pb-2">
                    <div className="mb-4">
                      {loginState ? <h3>Student</h3> : <h3>Sign Up</h3>}
                    </div>
                    <div className="social-login">
                      <a
                        href="#"
                        onClick={() =>
                          signIn("facebook", {
                            callbackUrl:
                              typeof window !== "undefined" &&
                              window.location.origin + "/#",
                          })
                        }
                        className="facebook"
                      >
                        <span className="icon-facebook ">
                          <img src="/icons/facebok.png" />
                        </span>
                      </a>
                      <a
                        href="#"
                        onClick={() =>
                          signIn("linkedin", {
                            callbackUrl:
                              typeof window !== "undefined" &&
                              window.location.origin + "/#",
                          })
                        }
                        className="twitter"
                      >
                        <span className="icon-twitter ">
                          <img src="/icons/linkedin.png" />
                        </span>
                      </a>
                      <a
                        href="#"
                        onClick={() =>
                          signIn("google", {
                            callbackUrl:
                              typeof window !== "undefined" &&
                              window.location.origin + "/#",
                          })
                        }
                        className="google"
                      >
                        <span className="icon-google ">
                          <img src="/icons/google.png" />
                        </span>
                      </a>
                    </div>
                    <span>or use your email account</span>
                  </div>

                  {loginState ? (
                    <>
                      <form onSubmit={login_formik.handleSubmit}>
                        <div className="form-group first">
                          {/* <label htmlFor="username">Username</label> */}
                          {isNumber == true ? (
                            <PhoneInput
                              country={"in"}
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              disableDropdown={true}
                              value={login_formik.values.email}
                              onChange={onNumberChange}
                            />
                          ) : (
                            <input
                              type="email"
                              className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                              placeholder="Email/Phone"
                              id="email"
                              name="email"
                              autoFocus={true}
                              onChange={handleChange}
                              value={login_formik.values.email}
                            />
                          )}
                          <div className="text-danger pt-1">
                            {login_formik.touched.email &&
                            login_formik.errors.email ? (
                              <div>{login_formik.errors.email}</div>
                            ) : null}
                          </div>
                        </div>
                        <button
                          type="submit"
                          disabled={loading}
                          className="no-icon default-btn btn-block"
                        >
                          {loading ? "Logging In.." : "Login With OTP"}
                        </button>
                      </form>
                      <div className="col-lg-12 pb-1 pt-1 text-center">
                        <span className="text-center"></span>
                      </div>
                      <button
                        type="submit"
                        onClick={() => {
                          setLoginState(!loginState);
                        }}
                        className="no-icon default-btn btn-block"
                      >
                        {loginState ? "New Student Sign Up" : "Login"}{" "}
                        <span></span>
                      </button>

                      <div
                        id="forgotPassword"
                        className="col-lg-12 col-md-12 col-sm-12  mt-2"
                      >
                        {/* <Link href="/reset-password"> */}
                        <a onClick={props.forgotToggle}>
                          Forgot Your Password?
                        </a>

                        {/* </Link> */}
                      </div>
                    </>
                  ) : (
                    <Signup
                      loginToggle={props.loginToggle}
                      successToggle={props.successToggle}
                      setuserState={props.setuserState}
                      settokenState={props.settokenState}
                      setLoginState={setLoginState}
                      loginState={loginState}
                      toggle={props.otpToggle}
                      verifyotpToggle={props.verifyotpToggle}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const LOGIN_YUP = Yup.object({
  email: Yup.string().email("Invalid email address").required("Required"),
});
const NUMBER_YUP = Yup.object({
  email: Yup.string()
    .min(10, "Must 10 characters long")
    // .max(15, "Must be equal to 10 characters ")
    .required("Required"),
});
