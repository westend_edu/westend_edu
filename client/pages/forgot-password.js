import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import baseUrl from "../utils/baseUrl";
import { useRouter } from "next/router";
import Axios from "axios";
import Notifier from "../utils/Notifier";
export default function Index(props) {
  const [isloading, setisloading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: LOGIN_YUP,
    onSubmit: async (values) => {
      setisloading(true);
      try {
        let response = await Axios({
          method: "put",
          url: `${baseUrl}/auth/forgot`,
          data: { email: values.email },
        });
        Notifier(response.data.message, "success");
        setisloading(false);
        props.forgotToggle();
        formik.handleReset();
      } catch (err) {
        Notifier(err.response.data.message, "error");
        setisloading(false);
      }
    },
  });
  return (
    <div className="otpLogin">
      <div className="loginContent">
        <div className="container">
          <div className="row">
            <div className="col-md-12 loginContents">
              <div className="row justify-loginContent-center">
                <div className="col-lg-8 offset-md-2">
                  <div className="col-lg-12 text-center pb-2">
                    <div className="mb-4">
                      <h3>Forgot Password</h3>
                      <p>Please enter your Email ID</p>
                    </div>
                  </div>

                  <form onSubmit={formik.handleSubmit}>
                    <>
                      <div className="form-group first">
                        {/* <label htmlFor="username">Username</label> */}
                        <input
                          type="email"
                          className="appearance-none block w-full  text-gray-700 border border-red-500 rounded py-3 px-4 mb-1 leading-tight focus:outline-none focus:bg-white"
                          placeholder="Email"
                          id="email"
                          name="email"
                          onChange={formik.handleChange}
                          value={formik.values.email}
                        />
                        <div className="text-danger pt-1">
                          {formik.touched.email && formik.errors.email ? (
                            <div className="formikError">
                              {formik.errors.email}
                            </div>
                          ) : null}
                        </div>
                      </div>
                    </>

                    <button type="submit" className="default-btn btn-block">
                      {isloading ? "SUBMITTING..." : "SUBMIT"}
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const LOGIN_YUP = Yup.object({
  email: Yup.string().email("Invalid email address").required("Required"),
});
