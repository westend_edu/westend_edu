import React from "react"

const StudentVisa = () => {
	return (
		<>
			<div className="about-area-three ptb-70">
				<div className="container">
					<div className="row">
						<div className="w-full text-center">
							<h1 className="">Scholarship</h1>
							<p className="pb-5">
								Dive deep in the pool of scholarships and get a diamond from the
								treasure
							</p>
						</div>
					</div>
					<div className="row">
						<div className="col-lg-12 col-md-12">
							<div className="about-content-box">
								<h3>Who Provides You With A Scholarship?</h3>
								<p>
									There are multiple firms and institutions that provide a range
									of scholarships to the potential students. These scholarships
									contain specific criterias to get eligible for the
									scholarship.
								</p>
								<h3>Who Can Apply For The Scholarship?</h3>
								<p>
									Anybody! Yes, anybody can apply for the scholarships.
									Scholarships are not just for scholars. It’s just that you
									need to search for the scholarship in which you pass their
									predefined eligibility criteria. Once applied, your
									application will take the further step.
								</p>
								<h3>Where To Apply For A Scholarship?</h3>
								<p>
									Westend is your end of the search. We have covered the latest,
									the oldest, the least and the fullest scholarships that can
									help you in lowering your burden of financial requirements. We
									have partnered with various companies who have numerous
									scholarships to offer. We believe in your dreams of higher
									studies and so we work very hard for you to achieve it.
								</p>
							</div>
							<div className="about-content-box">
								<h3>Explore The Scholarships</h3>

								<div className="blog-details-desc article-content ">
									<ul class="features-list">
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Country specific
												scholarship
											</h5>
											<p>
												Scholarships from various countries that are offered to
												you
											</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Popular scholarships
											</h5>
											<p>
												Number of students applied and get approved here. Now
												it's your chance.
											</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Deadline based
												scholarship
											</h5>
											<p>Hurry up before you miss out your scholarship</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Merit based Scholarship
											</h5>
											<p>If you are strong in merits, don’t miss out</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Scholarships from
												institutions
											</h5>
											<p>
												Your chosen college also has a scholarship for you.
												Check out
											</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Student specific
												scholarship
											</h5>
											<p>Are you talented beyond textbooks? Explore here</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Need based scholarship
											</h5>
											<p>
												Many are concerned for your dreams apart from you. Apply
												here
											</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Miscellaneous
												scholarship
											</h5>
											<p>
												Dig various scholarships and search for your suited one.
											</p>
										</li>
									</ul>
								</div>
								<h5>
									Still didn’t find a perfect one? Don’t worry our experts are
									here to help
								</h5>
							</div>
							<div className="about-content-box">
								<h3>Scholarship Process</h3>

								<h6>Get your assured scholarship through these simple steps</h6>

								<div className="blog-details-desc article-content ">
									<ul class="features-list">
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Introduce yourself
											</h5>
											<p>
												Let us know you and your scenario to help you out in
												finding a scholarship.
											</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Search
											</h5>
											<p>
												Search the scholarship where you can meet all their
												eligibility criteria
											</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Apply
											</h5>
											<p>
												Apply to your list of scholarship, submit the required
												documents and wait for approval
											</p>
										</li>
										<li className="flex flex-col">
											<h5 className="">
												<i class="bx bx-badge-check"></i>Approved
											</h5>
											<p>Congratulations you got your scholarship!</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="features-area w-full ptb-70 bg-f9fbff">
					<div className="container">
						<div className="row">
							<div className="col-lg-12 text-center">
								<h2 className="pb-10">
									<b>Our Lending Partners</b>
								</h2>
							</div>
						</div>
						<div className="row justify-content-center">
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/axis.jpg" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/avanse.jpg" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/auxilo.png" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/BOI.jpg" alt="image" />
								</div>
							</div>
						</div>

						<div className="row justify-content-center">
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/Canara.jpg" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/icici_bank.jpg" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/cridelo.png" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/mPower.png" alt="image" />
								</div>
							</div>
						</div>
						<div className="row ">
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/sbi.jpg" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/prodigy.jpg" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/incred.png" alt="image" />
								</div>
							</div>
							<div className="col-lg-3 col-sm-6 col-md-6">
								<div className="partners-box">
									<img src="/images/banks/leap-finance.png" alt="image" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default StudentVisa
