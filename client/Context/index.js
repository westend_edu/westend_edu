// Init
import React, { useContext, useState, createContext } from "react";
import cookie from "js-cookie";

// Initializing Create Context Hook
const storeContext = createContext();
const storeUpdateContext = createContext();
const fetchStoreContext = createContext();

// We are also initializing useContexts here then we only call these functions where we need that data
export function Store() {
  return useContext(storeContext);
}

export function UpdateStore() {
  return useContext(storeUpdateContext);
}

export function FetchStore() {
  return useContext(fetchStoreContext);
}

// Initializing Store Provider
export function StoreProvider({ children }) {
  // Initializing State

  let cooky = cookie.get("user");
  let token = cookie.get("token");

  let loggedInUser = cooky && JSON.parse(cooky);
  let [store, setStore] = useState({
    loading: false,
    isLogin: false,

    user: loggedInUser,
    token: token,
    role: undefined,
    loggedIn: false,
    users: [],
  });

  const updateStore = (data) => {
    setStore((prev) => {
      return {
        ...prev,
        ...data,
      };
    });
  };

  const fetchStore = (collection) => {
    fetch(setStore, collection);
  };

  // Render
  return (
    <storeContext.Provider value={store}>
      <storeUpdateContext.Provider value={updateStore}>
        <fetchStoreContext.Provider value={fetchStore}>
          {children}
        </fetchStoreContext.Provider>
      </storeUpdateContext.Provider>
    </storeContext.Provider>
  );
}
