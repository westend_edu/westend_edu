const Joi = require("joi")

const visaSchema = Joi.object({
    Name: Joi.string().pattern(/^[A-Za-z\s]*$/).required(),
    MobileNumber: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
    EmailId: Joi.string().email().required().lowercase(),
    City: Joi.string().required(),
    Country: Joi.string().required(),
    PreferredIntake: Joi.string().required(),
    LastQualification: Joi.string().required(),
    HighestQualification: Joi.string(),
    Percentage: Joi.string().pattern(/(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/),
    YearsOfExperience: Joi.string(),
    DesiredField: Joi.array(),
    EnglishTest: Joi.string(),
    EnglishTestScore: Joi.string()
})

const commentSchema = Joi.object({
    comment: Joi.string().required(),
    visaId: Joi.number().required()
})


module.exports = {
    visaSchema,
    commentSchema
}