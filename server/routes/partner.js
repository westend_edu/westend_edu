const express = require("express");
const router = express.Router();
const PartnerContoller = require("../controllers/partner");
const auth = require("../middlewares/auth");
router.get("/:id", PartnerContoller.getAllStudents);

router.post("/signup", PartnerContoller.signup);
router.post("/createPartnerLead", PartnerContoller.createPartnerLead);

router.post("/createStudent/:id", PartnerContoller.createStudent);
router.put("/updateStudent/:id", PartnerContoller.updateStudent);
router.delete(
  "/deleteStudent/:id",

  PartnerContoller.deleteStudent
);

router.get("/search/filter", PartnerContoller.getsearchStudent);
router.get("/", PartnerContoller.getPartner);
router.get("/search/searchByDate", PartnerContoller.searchByDate);

module.exports = router;
