const express = require("express")
const router = express.Router()

//for authentication
//pending for asking 
const auth = require("../middlewares/auth")
const VisaController = require("../controllers/visa")


router.post("/createVisaDetails", VisaController.createVisaDetails)
router.get("/getVisaDetails", VisaController.visaList)
router.get("/getVisaDetails/:id", VisaController.getVisaDetails)

module.exports = router