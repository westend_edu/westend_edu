const STRINGS = require("../utils/texts");
const response = require("../utils/response");
const { visaSchema } = require("../validators/joi/visa")
const prisma = require("../prisma/index")
const { getPaginationValues } = require("../utils/utilityservices")

class VisaController {
    // apply for the visa
    async createVisaDetails(req, res) {
        try {
            const ValidatorResult = await visaSchema.validateAsync(req.body)
            const result = await prisma.visa.create({
                data: {
                    name: ValidatorResult.Name,
                    mobileNumber: ValidatorResult.MobileNumber,
                    emailId: ValidatorResult.EmailId,
                    city: ValidatorResult.City,
                    country: ValidatorResult.Country,
                    preferredIntake: ValidatorResult.PreferredIntake,
                    lastQualification: ValidatorResult.LastQualification,
                    highestQualification: ValidatorResult.HighestQualification,
                    percentage: ValidatorResult.Percentage,
                    yearsOfExperience: ValidatorResult.YearsOfExperience,
                    desiredField: ValidatorResult.DesiredField,
                    englishTest: ValidatorResult.EnglishTest,
                    englishTestScore: ValidatorResult.EnglishTestScore
                }
            })
            return res.status(200).send(response(STRINGS.TEXTS.visaAppliedSuccessfully, result, true))

        }
        catch (err) {
            console.log(err.message);
            if (err.isJoi)
                return res.status(400).json({ error: STRINGS.ERRORS.invalidData, err })
            return res.status(500).json({ error: STRINGS.ERRORS.internalServerError })
        }
    }

    //visa list
    async visaList(req, res) {
        try {
            const { page, limit, sort, order, search } = getPaginationValues(req.query)
            let data
            if (search) {
                data = await prisma.visa.findMany({
                    where: {
                        OR: [
                            { name: { contains: search } },
                            { mobileNumber: { contains: search } },
                            { city: { contains: search } },
                            { emailId: { contains: search } }
                        ]
                    },
                    orderBy: [{ [`${sort}`]: `${order}` }],
                    skip: ((page - 1) * limit),
                    take: limit
                })
            } else {
                data = await prisma.visa.findMany({
                    orderBy: [{ [`${sort}`]: `${order}` }],
                    skip: ((page - 1) * limit),
                    take: limit
                })
            }
            if (!data.length)
                return res.status(200).json({ message: STRINGS.TEXTS.noData })
            const total = await prisma.visa.count()
            return res.status(200).json({ data, total })
        } catch (err) {
            return res.status(500).json({ error: STRINGS.ERRORS.internalServerError })
        }
    }

    // get the visa details
    async getVisaDetails(req, res) {
        try {
            const result = await prisma.visa.findFirst({
                where: { id: parseInt(req.params.id) }
            })
            if (!result)
                return res.status(400).send(response(STRINGS.ERRORS.visaNotFound, result))
            return res.status(200).send(response(STRINGS.TEXTS.visaFoundSuccessfully, result, true))
        }
        catch (err) {
            return res.status(500).json({ error: STRINGS.ERRORS.internalServerError })
        }
    }
}

module.exports = new VisaController()