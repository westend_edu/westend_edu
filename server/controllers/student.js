//Login User
const STRINGS = require("../utils/texts");
const response = require("../utils/response");
const prisma = require("../prisma");
const fs = require("fs");
var admin = require("firebase-admin");

class StudentContoller {
  async createCourseDetail(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      let {
        country,
        university,
        education,
        department,
        loanAmount,
        collateral,
        livingExpense,
        status,
        apply,
      } = req.body;
      let collateralNumber = req.body.collateralNumber.toString();
      let updatedUser;
      const user = await prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      let newId = user.id;

      if (user) {
        updatedUser = await prisma.application.update({
          where: {
            id: newId,
          },
          data: {
            country: country,
            university: university,
            education: education,
            department: department,
            loanAmount: loanAmount,
            livingExpense: livingExpense,
            collateral: collateral,
            collateralNumber: collateralNumber,
            status: status,
            apply: apply,
          },
        });
        const newUser = await prisma.application.findFirst({
          where: {
            userId: user.userId,
          },
        });
        await prisma.user.update({
          where: {
            id: newUser.userId,
          },
          data: {
            country: country,
          },
        });
      } else {
        res
          .status(400)
          .send(response(STRINGS.ERRORS.userNotFound, updatedUser));
      }

      res.status(200).send(response(STRINGS.TEXTS.courseDetails, updatedUser));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }

  //   create basic detail
  async createBasicDetail(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      let {
        firstName,
        lastName,
        phone,
        date,
        address,
        state,
        city,
        pinCode,
        email,
      } = req.body;
      const app = await prisma.application.findMany({});
      let updatedUser;

      if (app && app.length > 0) {
        const user = await prisma.application.findFirst({
          where: {
            userId: _id,
          },
        });

        if (user) {
          let newId = user.id;

          updatedUser = await prisma.application.update({
            where: {
              id: newId,
            },
            data: {
              firstName: firstName,
              lastName: lastName,
              phone: phone,
              date: date,
              address: address,
              city: city,
              email: email,
              state: state,
              pinCode: pinCode,
              app_status: "Application Started",
            },
          });
        } else {
          updatedUser = await prisma.application.create({
            data: {
              country: "",
              university: "",
              education: "",
              department: "",
              loanAmount: "",
              livingExpense: "",
              collateral: "",
              status: "",
              userId: _id,
              //   basic
              firstName: firstName,
              lastName: lastName,
              phone: phone,
              date: date,
              address: address,
              city: city,
              state: state,
              pinCode: pinCode,
              email: email,
              //
              degree: "",
              course: "",
              college: "",
              cgpa: "",
              tenPercentage: "",
              twelvePercentage: "",
              backlogs: "",
              backlogsNumber: "",
              name: "",
              relation: "",
              telephone: "",
              email: "",
              monthlyIncome: "",
              panNumber: "",
              adharNumber: "",
              app_status: "Application Started",
            },
          });
        }
      } else {
        updatedUser = await prisma.application.create({
          data: {
            country: "",
            university: "",
            education: "",
            department: "",
            loanAmount: "",
            livingExpense: "",
            collateral: "",
            status: "",
            userId: _id,
            //   basic
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            email: email,
            date: date,
            address: address,
            city: city,
            state: state,
            pinCode: pinCode,

            //
            degree: "",
            course: "",
            college: "",
            cgpa: "",
            tenPercentage: "",
            twelvePercentage: "",
            backlogs: "",
            backlogsNumber: "",
            name: "",
            relation: "",
            telephone: "",
            email: "",
            monthlyIncome: "",
            panNumber: "",
            adharNumber: "",
            app_status: "Application Started",
          },
        });
      }
      await prisma.user.update({
        where: {
          id: _id,
        },
        data: {
          app_status: "Application Started",
        },
      });
      res.status(200).send(response(STRINGS.TEXTS.courseDetails, updatedUser));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // *****
  //   create education detail
  async createEducationDetail(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      let {
        degree,
        backlogs,
        course,
        college,
        cgpa,
        tenPercentage,
        twelvePercentage,
        test,
        mortgage,
        backlogsNumber,
      } = req.body;
      let updatedUser;

      const user = await prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      let newId = user.id;
      if (user) {
        updatedUser = await prisma.application.update({
          where: {
            id: newId,
          },
          data: {
            degree: degree,
            test: test,
            mortgage: mortgage,
            course: course,
            college: college,
            cgpa: cgpa,
            tenPercentage: tenPercentage + "",
            twelvePercentage: twelvePercentage + "",
            backlogs: backlogs,
            backlogsNumber: backlogsNumber + "",
          },
        });
        const testScore = await prisma.testscore.findMany({
          where: {
            appId: newId,
          },
        });
        const appDocument = await prisma.appdocument.findMany({
          where: {
            appId: newId,
          },
        });
        let applicantDetail = await prisma.applicantsdetail.findFirst({
          where: {
            applicant: "1",
          },
        });
        updatedUser.applicantDetail = applicantDetail;

        updatedUser.testScore = testScore;
        updatedUser.appDocument = appDocument;
      } else {
        res
          .status(400)
          .send(response(STRINGS.ERRORS.userNotFound, updatedUser));
      }

      res.status(200).send(response(STRINGS.TEXTS.courseDetails, updatedUser));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // *****
  //   create application detail
  async createApplicantDetail(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      let {
        name,
        relation,
        telephone,
        email,
        monthlyIncome,
        panNumber,
        adharNumber,
        applicant,
      } = req.body;
      let loanEMI = "";
      let collateral = "";
      let collateralNumber = "";

      if (applicant == 1) {
        loanEMI = req.body.loanEMI;
        collateral = req.body.collateral;
        collateralNumber = req.body.collateralNumber.toString();
      }
      const user = await prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      let updatedUser = user;
      let appId = Number(user.id);

      let applicantDetail = await prisma.applicantsdetail.findFirst({
        where: {
          AND: [
            {
              appId: appId,
            },
            {
              applicant: applicant,
            },
          ],
        },
      });
      if (!applicantDetail) {
        applicantDetail = await prisma.applicantsdetail.create({
          data: {
            name: name,
            telephone: telephone,
            email: email,
            monthlyIncome: monthlyIncome,
            panNumber: panNumber,
            adharNumber: adharNumber,
            relation: relation,
            appId: appId,
            applicant: applicant,
            loanEMI: loanEMI,
            collateral: collateral,
            collateralNumber: collateralNumber,
          },
        });
      } else {
        let applicantDetailId = Number(applicantDetail.id);

        applicantDetail = await prisma.applicantsdetail.update({
          where: {
            id: applicantDetailId,
          },
          data: {
            name: name,
            telephone: telephone,
            email: email,
            monthlyIncome: monthlyIncome,
            panNumber: panNumber,
            adharNumber: adharNumber,
            relation: relation,
            appId: appId,
            applicant: applicant,
            loanEMI: loanEMI,
            collateral: collateral,
            collateralNumber: collateralNumber,
          },
        });
      }

      const testScore = await prisma.testscore.findMany({
        where: {
          appId: appId,
        },
      });
      const appDocument = await prisma.appdocument.findMany({
        where: {
          appId: appId,
        },
      });
      let applicantsDetail = await prisma.applicantsdetail.findFirst({
        where: {
          applicant: "2",
        },
      });
      updatedUser.testScore = testScore;
      updatedUser.appDocument = appDocument;
      updatedUser.applicantsdetail = applicantsDetail;
      updatedUser.testScore = testScore;

      res
        .status(200)
        .send(response(STRINGS.TEXTS.applicantDetails, updatedUser));
    } catch (err) {
      console.log(err.message, "Error->>");
      res.status(400).send(response(err.message));
    }
  }
  // *****
  //   create test score
  async createTestScore(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      let newUser = await prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (newUser) {
        let newId = newUser.id;
        console.log(req.body, "req.body");
        let { testName, score } = req.body;
        let updatedUser = await prisma.testscore.create({
          data: {
            testName: testName,
            score: score,
            appId: newId,
          },
        });
        const testScore = await prisma.testscore.findMany({
          where: {
            appId: newId,
          },
        });
        const appDocument = await prisma.appdocument.findMany({
          where: {
            appId: newId,
          },
        });
        let applicantDetail = await prisma.applicantsdetail.findFirst({
          where: {
            applicant: "1",
          },
        });
        newUser.applicantDetail = applicantDetail;

        newUser.testScore = testScore;

        newUser.appDocument = appDocument;
        res.status(200).send(response(STRINGS.TEXTS.testScore, newUser));
      } else {
        res.status(400).send(response(STRINGS.ERRORS.userNotFound));
      }
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // *****
  //   create document
  async createDocument(req, res) {
    try {
      let _id = JSON.parse(req.params.id);

      const user = await prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      const appUser = await prisma.user.findFirst({
        where: {
          id: _id,
        },
      });
      if (user) {
        let newId = Number(user.id);
        let { testName } = req.body;
        let UploadedPicture;

        if (req.file) {
          UploadedPicture = req.file.filename;
          let filePath = `${req.file.path}`;

          let destFileName =
            "applicationDocuments/" + appUser.email + "_" + req.file.filename;
          const options = {
            destination: destFileName,
            predefinedAcl: "publicRead",
          };
          let response = await admin
            .storage()
            .bucket()
            .upload(filePath, options)
            .then((result) => {
              const file = result[0];
              return file.getMetadata();
            });

          const metadata = response[0];
          var mediaLink = metadata.mediaLink;
        }
        let appId = Number(user.id);
        const app = await prisma.appdocument.findFirst({
          where: {
            AND: [
              {
                appId: appId,
              },
              {
                name: testName,
              },
            ],
          },
        });
        let updatedUser;
        if (app) {
          let documentID = Number(app.id);

          updatedUser = await prisma.appdocument.update({
            where: {
              id: documentID,
            },
            data: {
              name: testName,
              file: mediaLink,
              fileName: UploadedPicture,
              appId: appId,
            },
          });
        } else {
          updatedUser = await prisma.appdocument.create({
            data: {
              name: testName,
              file: mediaLink,
              fileName: UploadedPicture,
              appId: newId,
            },
          });
        }

        let doc = await prisma.appdocument.findMany({
          where: {
            appId: newId,
          },
        });
        const testScore = await prisma.testscore.findMany({
          where: {
            appId: newId,
          },
        });
        updatedUser.appDocument = doc;
        updatedUser.testScore = testScore;

        return res
          .status(200)
          .send(response(STRINGS.TEXTS.DocumentCreated, updatedUser));
      } else {
        return res.status(400).send(response(STRINGS.ERRORS.appNotFound));
      }
    } catch (err) {
      console.log(err.message);
      return res.status(400).send(response(err.message));
    }
  }
  async getApplication(req, res) {
    try {
      const _id = JSON.parse(req.params.id);

      // Check if user exist
      const app = await prisma.application.findFirst({
        where: {
          userId: _id,
        },
      });
      if (app) {
        const testScore = await prisma.testscore.findMany({
          where: {
            appId: app.id,
          },
        });
        const appDocument = await prisma.appdocument.findMany({
          where: {
            appId: app.id,
          },
        });

        let applicantDetail = await prisma.applicantsdetail.findMany({
          where: {
            appId: app.id,
          },
        });

        let result = {};
        result.applicantDetail = applicantDetail;

        result.data = app;
        result.testScore = testScore;

        result.appDocument = appDocument;
        res.status(200).send(response(STRINGS.TEXTS.getUser, result));
      } else {
        res.status(400).send(response("something went wrong"));
      }
    } catch (err) {
      res.status(400).send(response(err.message));
    }
  }
  //   create test score
  async deleteScore(req, res) {
    try {
      let _id = JSON.parse(req.params.id);

      let updatedUser = await prisma.testscore.delete({
        where: {
          id: _id,
        },
      });
      let appID = Number(updatedUser.appId);
      // Check if user exist
      const app = await prisma.application.findFirst({
        where: {
          id: appID,
        },
      });

      const testScore = await prisma.testscore.findMany({
        where: {
          appId: appID,
        },
      });
      app.testScore = testScore;
      res.status(200).send(response(STRINGS.TEXTS.testScoreDeleted, app));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // *****
  //   delete document
  async deleteDocument(req, res) {
    try {
      let _id = JSON.parse(req.params.id);

      let updatedUser = await prisma.appdocument.delete({
        where: {
          id: _id,
        },
      });
      res
        .status(200)
        .send(response(STRINGS.TEXTS.appDocumentDeleted, updatedUser));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
  // submit appplication

  async submitApplication(req, res) {
    try {
      let _id = JSON.parse(req.params.id);
      // Check if user exist
      const app = await prisma.user.update({
        where: {
          id: _id,
        },
        data: {
          app_status: req.body.app_status,
        },
      });
      const user = await prisma.application.findFirst({
        where: {
          userId: app.id,
        },
      });
      let appId = Number(user.id);
      await prisma.application.update({
        where: {
          id: appId,
        },
        data: {
          app_status: req.body.app_status,
        },
      });
      res.status(200).send(response(STRINGS.TEXTS.applicationSubmitted));
    } catch (err) {
      console.log(err.message);
      res.status(400).send(response(err.message));
    }
  }
}

module.exports = new StudentContoller();
