const nodemailer = require("nodemailer");
const CustomError = require("./../utils/customError");
const handlebars = require("handlebars");
const fs = require("fs");
const path = require("path");
const STRINGS = require("../utils/texts");
const ENV = process.env;
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
class MailService {
  constructor() {
    this.transporter = nodemailer.createTransport({
      service: ENV.MAILER_HOST,
      auth: {
        user: ENV.MAILER_SENDER_EMAIL,
        pass: ENV.MAILER_PASSWORD,
      },
    });
  }

  async resetEmail(to, token) {
    // Email Starts
    const filePath = path.join(
      __dirname,
      "../html_templates/requestResetPassword.html"
    );
    let link = `${process.env.CLIENT_URL}/reset-password/${token}`;
    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      link: link,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: Array.isArray(to.email) ? to.join() : to.email,
        subject: STRINGS.TEXTS.requestResetPasswordSubject,
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "reset email message"));

    return;
  }
  async sendOtpEmail(to, otp) {
    // Email Starts
    const filePath = path.join(
      __dirname,
      "../html_templates/sendOtpEmail.html"
    );
    let link = otp;
    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      link: link,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: Array.isArray(to.email) ? to.join() : to.email,
        subject: STRINGS.TEXTS.sendOtpSubject,
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "otp email message"));

    return;
  }

  async sendEmail(to, token) {
    // Email Starts
    let Subject;
    let filePath;
    let link;
    if (to.role == "partner") {
      filePath = path.join(__dirname, "../html_templates/partnerCreated.html");
      Subject = STRINGS.TEXTS.newPartnerCreated;
      link = `${process.env.CLIENT_URL}/verifyPartner/${token}`;
    } else {
      filePath = path.join(__dirname, "../html_templates/studentCreated.html");
      Subject = STRINGS.TEXTS.newStudentCreated;
      link = token;
    }

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      link: link,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: Array.isArray(to.email) ? to.join() : to.email,
        subject: "Welcome to Westend",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Signup email message"));

    return;
  }

  async sendEmailToStudent(to, token, title, content) {
    // Email Starts

    let filePath = path.join(
      __dirname,
      "../html_templates/emailToStudent.html"
    );

    let link = `${process.env.CLIENT_URL}/verifyUser/${token}`;

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      title: title,
      content: content,
      link: link,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: Array.isArray(to.email) ? to.join() : to.email,
        subject: title,
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Welcome email message"));

    return;
  }
  async sendEmailToPartner(to) {
    // Email Starts

    let filePath = path.join(
      __dirname,
      "../html_templates/welcomePartner.html"
    );

    let link = `${process.env.CLIENT_URL}/}`;

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      link: link,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: Array.isArray(to.email) ? to.join() : to.email,
        subject: "Welcome to Westend",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Welcome email message"));

    return;
  }
  async sendEmailToContactUS(to) {
    let filePath = path.join(__dirname, "../html_templates/contact.html");

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.name,
      email: to.email,
      phone: to.number,
      message: to.text,
    };
    const htmlToSend = template(replacements);
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: process.env.MAILER_RECIEVER,
        subject: "General Enquiry",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Enquiry email message"));

    return;
  }
  async sendEmailToStudentForApplicationReminder(to) {
    let filePath = path.join(
      __dirname,
      "../html_templates/applicationReminder.html"
    );

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);

    const replacements = {
      title: to.subject,
      content: to.content,
    };
    const htmlToSend = template(replacements);
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: to.email,
        subject: to.subject,
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Enquiry email message"));

    return;
  }

  async sendEmailNormal(to, title, content) {
    let filePath = path.join(
      __dirname,
      "../html_templates/emailToStudentNoVerify.html"
    );
    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      title: title,
      content: content,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: to,
        subject: title,
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "email message"));
    return;
  }

  async sendEmailRequestCallback(to) {
    let filePath = path.join(
      __dirname,
      "../html_templates/requestCallback.html"
    );
    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.name,
      email: to.email,
      phone: to.phone,
      amount: to.amount,
      date: to.date,
      time: to.amount,
      country: to.country,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: process.env.MAILER_RECIEVER,
        subject: "Callback Request",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) =>
        console.log(err.message, "request callback email message")
      );
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: to.email,
        subject: "Callback Request",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) =>
        console.log(err.message, "request callback email message")
      );
    return;
  }

  async sendEmailCheckEligibility(to) {
    let filePath = path.join(
      __dirname,
      "../html_templates/checkEligibilty.html"
    );
    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + to.lastName,
      email: to.email,
      phone: to.phone,
      amount: to.loanAmount,
      monthlyIncome: to.monthlyIncome,
      otherEMI: to.otherEMI,
    };
    const htmlToSend = template(replacements);

    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: process.env.MAILER_RECIEVER,
        subject: "Education Loan Enquiry",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) =>
        console.log(err.message, "Education Loan Enquiry email message")
      );
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: to.email,
        subject: "Education Loan Enquiry",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) =>
        console.log(err.message, "Education Loan Enquiry email message")
      );
    return;
  }

  async sendEmailToApplyScholarship(to) {
    let filePath = path.join(
      __dirname,
      "../html_templates/applyScholarship.html"
    );

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      phone: to.phone,
      university: to.university,
      country: to.country,
    };
    const htmlToSend = template(replacements);
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: process.env.MAILER_RECIEVER,
        subject: "Scholarship Request",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Enquiry email message"));

    return;
  }

  async sendEmailToForexRemmitance(to) {
    let filePath = path.join(
      __dirname,
      "../html_templates/forexRemittance.html"
    );

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      phone: to.phone,
      university: to.university,
      country: to.country,
      amount: to.currency + to.amount,
    };
    const htmlToSend = template(replacements);
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: process.env.MAILER_RECIEVER,
        subject: "Forex Request",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Enquiry email message"));

    return;
  }

  async sendEmailToStudentaccommodation(to) {
    let filePath = path.join(
      __dirname,
      "../html_templates/studentAccommodation.html"
    );

    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      phone: to.phone,
      university: to.university,
      country: to.country,
      amount: to.amount,
    };
    const htmlToSend = template(replacements);
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: process.env.MAILER_RECIEVER,
        subject: "Accommodation Request",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Enquiry email message"));

    return;
  }
  async sendEmailToSopReview(to) {
    let filePath = path.join(__dirname, "../html_templates/sopReview.html");
    const source = fs.readFileSync(filePath, "utf-8").toString();
    const template = handlebars.compile(source);
    const replacements = {
      name: to.firstName + " " + to.lastName,
      email: to.email,
      phone: to.phone,
      university: to.university,
      country: to.country,
      score: to.score,
      course: to.course,
    };
    const htmlToSend = template(replacements);
    await sgMail
      .send({
        from: process.env.MAILER_SENDER,
        to: process.env.MAILER_RECIEVER,
        subject: "Sop Review Request",
        html: htmlToSend,
      })
      .then(() => console.log("success"))
      .catch((err) => console.log(err.message, "Enquiry email message"));

    return;
  }
}

module.exports = MailService;
