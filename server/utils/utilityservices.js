const utilityServices = {}

utilityServices.getPaginationValues = (obj) => {
    let { page = 1, limit = 10, sort = "createdAt", order, search } = obj
    page = parseInt(page)
    limit = parseInt(limit)
    if (!order || (order !== "asc" && order !== "desc"))
        order = "asc"

    return { page, limit, sort, order, search }
}

module.exports = utilityServices