/*
  Warnings:

  - You are about to drop the column `prederedIntake` on the `Visa` table. All the data in the column will be lost.
  - You are about to drop the column `testScore` on the `Visa` table. All the data in the column will be lost.
  - Added the required column `englishTestScore` to the `Visa` table without a default value. This is not possible if the table is not empty.
  - Added the required column `percentage` to the `Visa` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Visa" DROP COLUMN "prederedIntake",
DROP COLUMN "testScore",
ADD COLUMN     "englishTestScore" TEXT NOT NULL,
ADD COLUMN     "percentage" TEXT NOT NULL,
ADD COLUMN     "preferredIntake" TEXT;
