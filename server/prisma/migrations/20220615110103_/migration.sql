/*
  Warnings:

  - You are about to drop the column `workExperience` on the `Visa` table. All the data in the column will be lost.
  - The `desiredField` column on the `Visa` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - Changed the type of `comments` on the `Visa` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "Visa" DROP COLUMN "workExperience",
DROP COLUMN "comments",
ADD COLUMN     "comments" JSONB NOT NULL,
DROP COLUMN "desiredField",
ADD COLUMN     "desiredField" JSONB[];
