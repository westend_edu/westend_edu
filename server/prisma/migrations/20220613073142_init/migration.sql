-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "firstName" TEXT NOT NULL DEFAULT E'',
    "lastName" TEXT NOT NULL DEFAULT E'',
    "role" TEXT DEFAULT E'student',
    "image" TEXT DEFAULT E'',
    "email" TEXT,
    "phone" TEXT,
    "password" TEXT,
    "company_name" TEXT NOT NULL DEFAULT E'',
    "address" TEXT NOT NULL DEFAULT E'',
    "website" TEXT NOT NULL DEFAULT E'',
    "reason" TEXT NOT NULL DEFAULT E'',
    "partnerId" INTEGER DEFAULT 0,
    "partner_name" TEXT NOT NULL DEFAULT E'',
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "resetPasswordToken" TEXT DEFAULT E'',
    "verifyPartnerToken" TEXT DEFAULT E'',
    "restPasswordExpires" TEXT DEFAULT E'',
    "passwordToken" TEXT DEFAULT E'',
    "passwordExpires" TEXT DEFAULT E'',
    "approved" TEXT NOT NULL DEFAULT E'pending',
    "isVerify" BOOLEAN NOT NULL DEFAULT true,
    "app_status" TEXT NOT NULL DEFAULT E'pending',
    "country" TEXT NOT NULL DEFAULT E'',
    "comment" TEXT NOT NULL DEFAULT E'',

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Partner" (
    "id" SERIAL NOT NULL,
    "company_name" TEXT NOT NULL DEFAULT E'',
    "address" TEXT NOT NULL DEFAULT E'',
    "website" TEXT NOT NULL DEFAULT E'',
    "reason" TEXT NOT NULL DEFAULT E'',
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Student" (
    "id" SERIAL NOT NULL,
    "partnerId" INTEGER DEFAULT 0,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PartnerLeads" (
    "id" SERIAL NOT NULL,
    "studentId" INTEGER,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AdminLeads" (
    "id" SERIAL NOT NULL,
    "studentId" INTEGER,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Otp" (
    "id" SERIAL NOT NULL,
    "otp" TEXT,
    "userId" INTEGER,
    "email" TEXT,
    "isExpired" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "testscore" (
    "id" SERIAL NOT NULL,
    "testName" TEXT,
    "score" TEXT,
    "appId" INTEGER,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "appdocument" (
    "id" SERIAL NOT NULL,
    "name" TEXT,
    "file" TEXT,
    "fileName" TEXT,
    "appId" INTEGER,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "applicantsdetail" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL DEFAULT E'',
    "relation" TEXT NOT NULL DEFAULT E'',
    "telephone" TEXT NOT NULL DEFAULT E'',
    "email" TEXT NOT NULL DEFAULT E'',
    "monthlyIncome" TEXT NOT NULL DEFAULT E'',
    "panNumber" TEXT NOT NULL DEFAULT E'',
    "adharNumber" TEXT NOT NULL DEFAULT E'',
    "loanEMI" TEXT NOT NULL DEFAULT E'',
    "applicant" TEXT NOT NULL DEFAULT E'',
    "collateral" TEXT NOT NULL DEFAULT E'No',
    "collateralNumber" TEXT NOT NULL DEFAULT E'0',
    "appId" INTEGER,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Application" (
    "id" SERIAL NOT NULL,
    "userId" INTEGER,
    "otherEMI" TEXT NOT NULL DEFAULT E'',
    "app_status" TEXT NOT NULL DEFAULT E'pending',
    "country" TEXT NOT NULL DEFAULT E'',
    "university" TEXT NOT NULL DEFAULT E'',
    "education" TEXT NOT NULL DEFAULT E'',
    "department" TEXT NOT NULL DEFAULT E'',
    "loanAmount" TEXT NOT NULL DEFAULT E'',
    "livingExpense" TEXT NOT NULL DEFAULT E'No',
    "collateral" TEXT NOT NULL DEFAULT E'No',
    "collateralNumber" TEXT NOT NULL DEFAULT E'0',
    "firstName" TEXT NOT NULL DEFAULT E'',
    "lastName" TEXT NOT NULL DEFAULT E'',
    "phone" TEXT NOT NULL DEFAULT E'',
    "email" TEXT NOT NULL DEFAULT E'',
    "date" TEXT NOT NULL DEFAULT E'',
    "address" TEXT NOT NULL DEFAULT E'',
    "state" TEXT NOT NULL DEFAULT E'',
    "city" TEXT NOT NULL DEFAULT E'',
    "pinCode" TEXT NOT NULL DEFAULT E'',
    "status" TEXT NOT NULL DEFAULT E'',
    "degree" TEXT NOT NULL DEFAULT E'',
    "course" TEXT NOT NULL DEFAULT E'',
    "college" TEXT NOT NULL DEFAULT E'',
    "cgpa" TEXT NOT NULL DEFAULT E'',
    "tenPercentage" TEXT NOT NULL DEFAULT E'',
    "twelvePercentage" TEXT NOT NULL DEFAULT E'',
    "backlogs" TEXT NOT NULL DEFAULT E'No',
    "backlogsNumber" TEXT NOT NULL DEFAULT E'0',
    "test" TEXT NOT NULL DEFAULT E'No',
    "mortgage" TEXT NOT NULL DEFAULT E'',
    "apply" TEXT NOT NULL DEFAULT E'PG',
    "name" TEXT NOT NULL DEFAULT E'',
    "relation" TEXT NOT NULL DEFAULT E'',
    "telephone" TEXT NOT NULL DEFAULT E'',
    "monthlyIncome" TEXT NOT NULL DEFAULT E'',
    "panNumber" TEXT NOT NULL DEFAULT E'',
    "adharNumber" TEXT NOT NULL DEFAULT E'',
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Otp.email_unique" ON "Otp"("email");

-- CreateIndex
CREATE UNIQUE INDEX "Otp_userId_unique" ON "Otp"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "Application_userId_unique" ON "Application"("userId");

-- AddForeignKey
ALTER TABLE "PartnerLeads" ADD FOREIGN KEY ("studentId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AdminLeads" ADD FOREIGN KEY ("studentId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Otp" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "testscore" ADD FOREIGN KEY ("appId") REFERENCES "Application"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "appdocument" ADD FOREIGN KEY ("appId") REFERENCES "Application"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Application" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
