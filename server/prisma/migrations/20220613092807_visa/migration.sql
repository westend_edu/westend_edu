-- CreateTable
CREATE TABLE "Visa" (
    "id" SERIAL NOT NULL,
    "name" TEXT,
    "mobileNumber" TEXT,
    "emailId" TEXT,
    "city" TEXT,

    PRIMARY KEY ("id")
);
