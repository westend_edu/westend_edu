/*
  Warnings:

  - The `desiredField` column on the `Visa` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Visa" DROP COLUMN "desiredField",
ADD COLUMN     "desiredField" TEXT[];
