/*
  Warnings:

  - Added the required column `desiredField` to the `Visa` table without a default value. This is not possible if the table is not empty.
  - Added the required column `englishTest` to the `Visa` table without a default value. This is not possible if the table is not empty.
  - Added the required column `testScore` to the `Visa` table without a default value. This is not possible if the table is not empty.
  - Added the required column `yearsOfExperience` to the `Visa` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Visa" ADD COLUMN     "country" TEXT,
ADD COLUMN     "desiredField" TEXT NOT NULL,
ADD COLUMN     "englishTest" TEXT NOT NULL,
ADD COLUMN     "highestQualification" TEXT,
ADD COLUMN     "lastQualification" TEXT,
ADD COLUMN     "prederedIntake" TEXT,
ADD COLUMN     "testScore" TEXT NOT NULL,
ADD COLUMN     "workExperience" TEXT,
ADD COLUMN     "yearsOfExperience" TEXT NOT NULL;
