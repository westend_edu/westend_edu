/*
  Warnings:

  - Made the column `name` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `mobileNumber` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `emailId` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `city` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `country` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `highestQualification` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `lastQualification` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `workExperience` on table `Visa` required. This step will fail if there are existing NULL values in that column.
  - Made the column `preferredIntake` on table `Visa` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Visa" ALTER COLUMN "name" SET NOT NULL,
ALTER COLUMN "mobileNumber" SET NOT NULL,
ALTER COLUMN "emailId" SET NOT NULL,
ALTER COLUMN "city" SET NOT NULL,
ALTER COLUMN "country" SET NOT NULL,
ALTER COLUMN "desiredField" DROP NOT NULL,
ALTER COLUMN "desiredField" SET DEFAULT E'NA',
ALTER COLUMN "englishTest" DROP NOT NULL,
ALTER COLUMN "englishTest" SET DEFAULT E'NA',
ALTER COLUMN "highestQualification" SET NOT NULL,
ALTER COLUMN "lastQualification" SET NOT NULL,
ALTER COLUMN "workExperience" SET NOT NULL,
ALTER COLUMN "yearsOfExperience" DROP NOT NULL,
ALTER COLUMN "yearsOfExperience" SET DEFAULT E'NA',
ALTER COLUMN "englishTestScore" DROP NOT NULL,
ALTER COLUMN "englishTestScore" SET DEFAULT E'NA',
ALTER COLUMN "percentage" DROP NOT NULL,
ALTER COLUMN "percentage" SET DEFAULT E'NA',
ALTER COLUMN "preferredIntake" SET NOT NULL;
